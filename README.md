# relearn-frontend

L'objectif est de parcourir la matière contenu dans l'issue #1 afin de me remettre à jour en programmation frontend.

Cela couvre en substance : 


* [ ] HTML/DOM
* [ ] CSS basics : structure, selectors
* CSS Advanced : 
  * [ ] CSS Flex
  * [ ] CSS Grid
  * [ ] CSS Variables
  * [ ] CSS transitions
  * [ ] CSS animations
  * [ ] CSS preprocessor (SASS, PostCSS)
* [ ] Responsive Web (viewport meta, etc)
* Javascript : 
  * [ ] Basics (values, types, statements, controls structures, functions)
  * [ ] objects and arrays
  * [ ] advanced topics on functions (higher order functions, etc)
  * [ ] OO in javascript : object life cycle, classes, prototypes
  * [ ] errors handling : debuging, exception, strict mode, etc
  * [ ] modules creating, importing (require, es6 import)
  * Asynchronous in javascript :
    * [ ] callbacks 
    * [ ] promises
    * [ ] async, events
  * [ ] DOM : Finding, editing, creating nodes in Document
  * [ ] DOM events, propagation, 
* Tooling from Javascript ecosystem :
  * [ ] npm/yarn
  * [ ] webpack
* VueJS : 
  * [ ] Vue basics
  * [ ] Vue router
  * [ ] vuex (store global state)
* [ ] Google material design guidelines + vuetify

