# Javascript ecosystem

Le monde JS foisonnent d'initiative et évolue très vite.

Voici quelques outils standards actuels

## npm / yarn

Permet de définir les dépendances d'un projet et de les télécharger en local, dans le répertoire
nodes_modules du projet.

yarn est le successeur de npm: il palliait nombre de ses limitations, mais depuis qu'une nouvelle version de npm existe, tout n'est plus aussi clair.

Le fichier `package.json` définit les diverses dépendances en terme de contrainte de version (version stricte, ou min, ou max) : Par example

```json
{
  "name": "frontend",
  "version": "0.1.0",
  "private": true,
  "scripts": {
    "serve": "vue-cli-service serve",
    "build": "vue-cli-service build"
  },
  "dependencies": {
    "axios": "^0.19.0",
    "core-js": "^3.4.0",
    "vue": "^2.6.10",
    "vue-router": "^3.0.3",
    "vuex": "^3.0.1"
  },
  "devDependencies": {
    "@vue/cli-plugin-babel": "^4.0.5",
    "@vue/cli-service": "^4.0.5",
    "vue-template-compiler": "^2.6.10"
  }
}
```

Ensuite, en faisant `yarn install`, les dépendances sont résolues et installée, puis un fichier `yarn.lock` est créé, qui indique précisément quelle versions de chaque package est installée.

Ensuite, si on veut réinstaller les dernières dépendances du moment, on peut faire `yarn upgrade --latest` ou alors `rm yarn.lock && yarn intall`

## Babel

https://babeljs.io permet de transpiler du javascript moderne (de la dernière spec, voir du typescript) en "vieux" javascript, s'assurant ainsi la compatibilité avec d'anciens navigateurs.

Cela à tout de même l'effet de bord particulier qui est que dorénavant, le javascript qui s'exécute dans les browser, n'est plus exactement celui qu'on a écrit, mais le résultat d'une transpilation ce qui peut poser des problèmes durant le deboggage.

Example d'utilisation : en ligne de commande.

```bash
mkdir myproject
cd myproject
yarn add @babel/core @babel/cli @babel/preset-env --dev
```

Créer le fichier d'entrée, p.ex mon_code.js
```js

// Un fichier JS qui utilise quelques construction modernes

const my_func => arr => {
    return arr.map( x => x ** 2);
};

console.log(my_func([1,2,3]));

```

créer le fichier de configuration `babel.config.js` :

```js
module.exports = function (api) {

  // Only execute this file once and cache the resulted config object below for the next babel uses.
  // more info about the babel config caching can be found here: https://babeljs.io/docs/en/config-files#apicache
  api.cache.using(() => process.env.NODE_ENV === "development")

  return {
    presets: [
      // Use the preset-env babel plugins
      '@babel/preset-env'
    ]
  }
}
```

Ce 'preset' javascript permet de définir un niveau de compatibilité en terme de marché de navigateur.
Ceci ce spécifie via le fichier `.browserlistrc` :

```js
> 1%
last 5 versions
```

Ensuite on lance `npx babel mon_code.js --out-file mon_code_es5.js`

et on constate que `mon_code_es5.js` est compatible avec les vieux browsers :

```js
"use strict";

// Un fichier JS qui utilise quelques construction modernes
var my_func = function my_func(arr) {
  return arr.map(function (x) {
    return Math.pow(x, 2);
  });
};

console.log(my_func([1, 2, 3]));
```

## polyfill

Il s'agit de librairies qu'on importe en début d'exécution de notre code JavaScript
qui permet d'émuler par injection de prototype des fonctionalités de javascript récentes
au sein d'environnement anciens.

Le plus connu est https://github.com/zloirock/core-js

## npm/yarn/npx

yarn et npm sont des outils similaires qui installent des dépendances spécifiée dans le fichier `package.json` pour les installer dans `./node_modules`. Il me semble que yarn a une approche plus moderne.

Il faut noter que package.json sépare ses dépendances en deux sections :
- les dépendance 'normales' de l'application
- les dépendances de développement: ces dernières ne seront pas bundlée avec l'application

Commandes courantes :
- `yarn add ma_dep` : ajoute une dépendance dans package.json dans "dependencies" et l'installe dans node_modules du projet local
- `yarn add ma_dep --dev` : idem, mais la met dans la section "devDependencies"
- `yarn upgrade` : met à jour les packages et va chercher les version les plus récentes
- `yarn install` : résoud les dépendances (si yarn.lock n'existe pas) et installe les modules dans node_modules
- `yarn global add ma_dep` : Installe un module en mode 'global' (see below)

**Local/global** et cas particulier des modules NPM avec **pseudo binaires**:

Quand on installe un module NPM, ce dernier peut être installé en local (dans le ./node_modules courant) ou en "global", c.a.d. accessible depuis l'extérieur.

Attention, yarn et npm on des approches différentes à l'installation globale :
- npm installe dans /usr/lib/node_modules (ou /usr/local) -> Il faut donc les **droits SU** !!
- yarn a une approche différente et par défaut, installe dans le home directory (cela peut être overridé) (see https://yarnpkg.com/lang/en/docs/cli/global/). Par défaut, yarn install dans ~/.yarn/bin ... répertoire a mettre dans le path

Certains modules peuvent être des commandes systèmes qui commence par `#!/usr/bin/env node` ... si elle sont installées en global, le binaire qui en découle se trouvera p.ex dans ~/.yarn/bin, mais si on installe en local, il sera dans le sous-répertoire du projet, dans ./node_modules. Si on veut l'exécuter sans s'embêter à chercher le chemin, on peut utiliser `npx`

P.ex
```bash
cd ~/tmp
mkdir monprojet
cd monprojet
yarn add react-cli
$> react-cli
Unknown command react-cli
$> ./node_modules/react-cli/bin/react --version
0.3.1
$> npx react-cli --version
0.3.1
```

## WebPack

C'est un outils très utilisé dans le monde JS dont les fonctionalités s'articulent autour de:

1. module bundling : Permet d'analyser les dépendances d'un ensemble de fichier javascript et d'en créer des fichiers autonome pour les importer lors du déploiement de l'application. La création de plusieurs fichier permet d'avoir une application frontend qui se charge qu'à la demande. Il sait supporter tous les standards de modules.

2. assets pipeline processing : Permet d'appliquer des transformation (Babel, CSS, etc) à la chaine d'assets.

3. Permet d'avoir un serveur web de développement supportant le module hotreload pour le développeur.


Toutes ces fonctionalités sont utilisées de manières quasi-transparentes par les outils de commande en ligne des différents
framework front comme vueJS qui font un usage **intensif** de webpack.

See https://webpack.js.org/guides/getting-started/

### Example

Le projet dans [ce repertoire](./examples/webpack) a été initialisé comme ceci :

```bash
cd ~/tmp
mkdir monprojet

yarn add webpack webpack-cli --dev
```

Le javascript que l'on veut exécuter dans notre navigateur à deux dépendances :

1. Une locale, sous la forme de src/libs/ma_lib
2. Une via un module NPM, pex lodash, une librairie qui facilite la programmation functionnelle.

A noter :
- webpack se configure via webpack.config.js
- webpack a besoin d'un point d'entrée de notre javascript
- webpack doit savoir où sauver le fichier bundle résultant.
- Les assets statiques de notre projet (le fichier html et le bundle js) seront dans dist

Faire un checkout du projet :
- L'ouvrir dans WebStorm et explorer
- faire `yarn run build`, puis avec un navigateur, ouvrir le fichier dist/index.html et inspecter avec les dev tools
- On peut modifier dans webpack.config.js le 'mode' en production, refaire `yarn run build` et comparer la taille du main_bundle.js
