# Async programming in Javascript

Avec son évolution, Javascript supporte différent modèle de programmation asynchrone :

- callbacks
- promises
- async/await

## callbacks functions

C'était la façon classique d'exécuter du code asynchrone : on passe une référence sur une fonction a appeler quand l'état asynchrone est atteint. Ce qui permet d'attendre la fin de l'exécution de code qui prend du temps, sans pour autant bloquer le flux d'exécution de l'application.

```js
setTimeout(() => console.log("Tick!"), 1000); // or setTimeout(function() { console.log("Tick"); }, 1000);
console.log("timeOut called ... waiting for callback");

//timeOut called ... waiting for callback
//Tick !
```

```js
document.getElementById('button').addEventListener(
    'click',
    () => {
          console.log("Le bouton a été cliqué !");
          }
    );
```

See also https://codepen.io/jfburdet/pen/eYYMxJy

**Callback hell**

Les callback posent des problèmes quand on les imbriquent et rende le code difficilement lisible et debugable

```js
window.addEventListener('load', () => {
  document.getElementById('button').addEventListener('click', () => {
      setTimeout(() => {
          items.forEach(item => {
            //your code here
          })
      }, 2000)
  })
});
```


C'est pour cela que les specs suivantes de javascript ont amené de nouveaux constructeurs plus élégants: *les promises*.

## Les promises
https://javascript.info/async

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise
https://medium.com/@ramsunvtech/promises-of-promise-part-1-53f769245a53

Les promises sont le nouveaux constructeur standard de code asynchrone de javascript. Elle représente le fait qu'une opération pourra être réussie, ou échouée, cela de manière asynchrone.

La structure générale est la suivante :
- On instantie une Promise, qui est alors a l'état `pending`
- Le code asynchrone s'exécute : et soit il est un succès, et appelle le call back `resolve` avec une éventuelle valeure. Soit il y a eu un problème, et il appelle `reject` avec en paramètre le code d'erreur.

L'exemple suivant décrit la structure générale:

```js

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// Instantiation du Promise
let mypromise = new Promise(
                  async (resolve, reject) => {

                     //On simule un sleep par un promise ...
                      await sleep(5000);


                      console.log("La promesse a fini")

                      //On simule est échec ou succès aléatoire
                      let result = Math.random();
                      if (result >= 0.5) {
                        console.log("Promesse réussi: calling resolve")
                        resolve(result)
                      } else {
                        console.log("Promesse échouée: calling reject")
                        reject(`Une erreure est arrivée ${result}`);
                      }
                  }
                );

// La promise est maintenant pending
// On va attendre son résultat
console.log("La promise a été instantiée")

//Consomation du Promise

mypromise.then( (success_value) => {
    console.log("Le promise a réussi: " + success_value);
}).catch( (error_value) => {
  console.log("Le promise a échoué: " + error_value);
}
);

/*
Sample output :
La promise a été instantiée
La promesse a fini
Promesse échouée: calling reject
Le promise a échoué: Une erreure est arrivée 0.49373733713620926
*/
```

### Chainage des Promises

Une promise peut retourner un promise et on peut chainer les statement .then.
Une bonne illustration est d'utiliser l'API fetch :

```js
fetch('https://jsonplaceholder.typicode.com/todos/1')
  .then(response => response.json())
  .then(json => console.log(json))

// {
//   "userId": 1,
//  "id": 1,
//  "title": "delectus aut autem",
//  "completed": false
// }

fetch('https://jsonplaceholder.typicode.com/todos/1')
  .then(response => response.json())
  .then(json_object => console.log(`json.title = ${json_object.title}`))
console.log("Le fetch est lancé, la réponse va arriver : ");  

// Le fetch est lancé, la réponse va arriver : debugger eval code:4:9
// json.title = delectus aut autem
```

### Traitement groupé de Promise

La librairie Promise contient plusieurs méthode qui permettent de traiter un
groupe de promise :

P.ex Promise.all() https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Promise/all

## async/await

Ces constructeurs sont la dernière évolutions pour permettre de faciliter l'écriture
de code asynchrone. **async/await est construit par dessus les promises**. Ces dernières restent
donc des éléments centraux du language.

Ces constructeurs rendent le code plus clair en permettant de remplacer p.ex le chainage de .then
en appel séquentiels, qui seront plus facile à debogguer.

En précédent une fonction du mot-clé **async** cette dernière encapsule la valeur de retour dans
une promise :

```js
async function f() {
  return 1;
}

f().then(res => console.log(`Result is ${res}`));  // Result is 1

// est equivalent a

async function f() {
  return Promise.resolve(1);
}
```

On utilise le mot-clé **await** uniquement dans une fonction **async**. Await prend comme argument une Promise
il va attendre (sans consommer de resources) que la Promise se settle :

```js
async function f() {
  return 1;
}

const consumer = async () => {  
  let result = await f();
  console.log(`Result found ${result}`);
}

consumer();
// Result found 1
```


# Modules

Historiquement, Javascript à eu plusieurs façon de packager des librairies en entités indépendantes, la principale étant CommonJS pour le monde NodeJS.

En gros, c'est un peu le bordel : https://dev.to/iggredible/what-the-heck-are-cjs-amd-umd-and-esm-ikm

Avec ES6, les browser on enfin un façon standard d'importer des modules, basée sur les mots clés `import` et `export`, qui est appelé `ES Modules` ou `ESM`.

Le principe reste similaire a ce qui se faisant : Une dépendances (module) est importée par le mot-clé "import". L'importation peut être faite en provenance du code source local ou d'un package externe installé par un package manager (npm/yarn) dans le répertoire `node_modules`.

Dans un projet frontend typique (p.ex basé sur VueJS) les dépendances externes sont spécifiée dans le fichier `package.json`, dans la section `dependencies`, puis sont transpilée (par Babel) et bundlée (par Webpack).

On note que le mot-clé `default` permet de n'exporter qu'un seul objet ou de définir celui qui est par défaut.

Exemple de modules ESM :


```js

// --------------------------------------------
// Fichier malib.js
const au_cube = (x) => {
  console.log(`cube(${x})`)
  return x * x * x;
}

const TWOPI = Math.PI * 2;
const UNCHIFFRE = 35 * TWOPI;

export {au_cube, TWOPI};
export default au_cube;
//----------------------------------------------

// Fichier malib_consumer1.js
import {au_cube, TWOPI} from "./malib"
console.log(`Le cube de 3 vaut ${au_cube(3)}`);
console.log(`Deux fois PI vaut ${TWOPI}`);
// >> cube(3)
// >> Le cube de 3 vaut 27
// >> Deux fois PI vaut 6.283185307179586

//----------------------------------------------
// Fichier malib_consumer2.js
import malib_def_function from "./ma_lib";
console.log(`Le fonction par defaut de 3 de ma_lib : ${malib_def_function(3)}`);
// >> cube(3)
// >> Le fonction par defaut de 3 de ma_lib : 27
```
