## Closure

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures

En javascript, les fonctions peuvent être traitée comme des valeurs et passées
dans le flux d'exécution du programme. Ces fonctions gardent "souvenir" de
l'environnement d'exécution où elles ont été crées :

```js

function adder(x) {
  return function(y) {
    return x + y;
  };
};

let add5 = adder(5);
let add10 = adder(10);

add5(1);
>>> 6

add10(10);
>>> 20

```

**Example avancé**

Emulation de méthode privée. Comme JS n'a historiquement pas de constructeur add-hoc,
ce petit monde s'est débrouillé pour reproduire des patterns OO par exemple à coup de closure :

```js
var counter = (function() {
  var privateCounter = 0;
  function changeBy(val) {
    privateCounter += val;
  }
  return {
    increment: function() {
      changeBy(1);
    },
    decrement: function() {
      changeBy(-1);
    },
    value: function() {
      return privateCounter;
    }
  };
})();

console.log(counter.value()); // logs 0
counter.increment();
counter.increment();
console.log(counter.value()); // logs 2
counter.decrement();
console.log(counter.value()); // logs 1
```

Explication :
- counter sera affecté à la valeur retournée par l'exécution immédiate (syntatxe IIFE) de la fonction.
- La fonction retourne un objet qui a les propriété de type fonction :increment, decrement, value
- Ces fonctions sont des closures et se souviennent de leur scope qu'elles partagent.
- counter devient donc le binding sur lequel on peut invoquer ces méthodes.

## Quelques examples de functional programming

```js
function repeatMe(count, action) {
  for (const c of Array(count).keys()) {
    action();
  };
};

// A essayer en nodejs
repeatMe(3, () => { console.log("Salut !")});
>>> Salut !
>>> Salut !
>>> Salut !
```


```js
function filter(array, test) {
  let passed = [];
  for (let element of array) {
    if (test(element)) {
      passed.push(element);
    }
  }
  return passed;
}

let input = ["JF", "Chloé", "Yoan"];

let my_filter = arg => {
  return arg ==="JF";
}

let res = filter(input, my_filter);

console.log(res);

>>> ['JF']

```

## Functional programming on arrays

En regardant https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array ou en faisant :

```js
let a = [];
console.log(a.__proto__);
```

On reconnait les fonctions classiques qui agissent sur des énumérations: filter, find, map, reduce, some, etc.

pex.

```js
let input = [1,2,3];
console.log(input.map( x => x * x ))
>>> Array(3) [1, 4, 9]
```

## Generator

C'est le même principe qu'en python, c'est une fonction qui génère un itérateur. On doit utiliser les éléments l'astérisque `*` et le mot clé `yield`.

```js
function* idMaker(){
    var index = 0;
    while(index<10)
        yield index++;
}

let gen1 = idMaker();
let gen2 = idMaker();

console.log("Consommation via trois next()")
console.log(gen1.next().value); // 0
console.log(gen1.next().value); // 1
console.log(gen1.next().value); // 2

console.log("\n Consommation via une seule loop")
for (let val of gen2) {
  console.log(`gen2 in loop : val = ${val}`);
}
```
