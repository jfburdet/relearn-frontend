# Javascript basics

## Learning resources

- https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide
- https://www.digitalocean.com/community/tutorial_series/how-to-code-in-javascript
- [Eloquent Javascript](resources/Eloquent_JavaScript.pdf)
- [Flavio Copes JavaScript Handbook](resources/javascript-handbook.pdf)
- [Flavio Copes From ES5 to EsNext](resources/fromES5_to_EsNext.pdf)


## Javascript versions

Plusieurs version de Javascript se sont succédées : https://www.w3schools.com/js/js_versions.asp

En date de l'écriture de ce document (oct2019) la dernière version supportée par la majorité des navigateurs moderne est **ES6 / ES2015** (See https://caniuse.com/#search=es6) depuis 2016.

Il faut noter que ES7 n'apporte que deux choses mineures: https://flaviocopes.com/es2016/

Le présent tutorial couvrira ES7.

Il faut noter qu'on peut toujours utiliser la dernière version de javascript, et utiliser https://babeljs.io/ dans la chaîne de transpilation. Cette façon de faire est d'ailleurs standard avec VueJS.

Pour étendre les fonctionnalités d'un navigateurs à une version javascript qu'il n'utilise pas en natif, il existe également la notion de https://en.wikipedia.org/wiki/Polyfill_(programming)

Il est naturellement recommandé d'utiliser un linter pour avoir un style de code standardisé, sans erreur et propre.

A noter que la tendance est maintenant d'utiliser de plus en plus des super-set de javascript, tel que typescript, qui permet de typer plus fortement le language. La prochaine version de VueJS sera écrite complètement en TS. En cas d'usage du TS, sa compilation prend place dans la même chaine que l'utilisation de babel.

## Valeurs et types

Pour tester, on peut ouvrir un navigateur, lancer les dev tools (F12) et utiliser la **console**.

En JS, chaque donnée est une **value**, qui possède un **type**.

Le mot clé **typeof** permet de déterminer le type.

Le type possible sont les suivants :

```js
typeof(2);
 -> "number"
```

```js
typeof("toto");
 -> "string"
```

```js
typeof(2);
 -> "undefined"
```

```js
typeof(null);
 -> "object"  //Ce type sera couvert dans prochains chapitres.
```
https://flaviocopes.com/javascript-destructuring/
```js
typeof(undefined);
 -> "undefined"
```

```js
typeof(2 > 3);
 -> "boolean"
```

### Les nombres spéciaux
- `Infinity`
- `-Infinity`
- `NaN`

### Les strings

On peut utiliser le simple quote ('), le double (") ou le backticks (``` ` ```)

On peut utiliser le backslach pour encoder pex le retour de ligne `\n`

Avec le backticks, on peut faire de l'interpolation :

    console.log(`half of 100 is ${100 / 2}`)
    -> "half of 100 is 50"


### Les types vide

`null` et `undefined` qui sont essentiellement interchangeables.

### Conversion de type

En JS, si on ne fait pas attention, on peut se trouver à subir des conversions de type implicites qui sont sources de bugs. (type coercion)

    console.log(8 * null)
    // → 0
    console.log("5" - 1)
    // → 4
    console.log("5" + 1)
    // → 51
    console.log("five" * 2)
    18// → NaN
    console.log(false == 0)
    // → true

### Boolean shortcuit

Permet souvent d'établir un fallback value, si un des opérateur est null.

Un peu tordu :
- Convertit les arguments en booleen : ce qui n'est pas explicitement boolean, est convertit :
  - tout ce qui est null ou undefined est `false`
  - toute valeur définie (string, number) est `true`
- Selon la valeur booleenne retourne la **valeur originale** (sans la conversion) de l'argument


    var toto; //undefined
    console.log(toto || "user")
    // → user

    console.log("Agnes" || "user")
    // → Agnes    


## Program structure

### Expression et statements

Expression -> retourne valeur

On peut agréger des expression pour construire des `statement`, donc la liste constitue le programme.

Les statements se terminent par un **point-virgule**.

### Assignements (binding)

Quand on affecte une valeur à un symbole (variable) et qu'on veut que le scope soit local au bloc on utilise :

```js
let toto = "salut toi";
```

Si c'est une valeur constante :
```js
const toto = "salut toi";
```


On peut aussi utiliser  `var` à la place de `let`, sachant que `var` déclare des variables qui tendent à être plus globale.

**On peut retenir comme règle** que quand on déclare qqchose, on utilise la chose la plus locale/invariable et var en dernier ressort :
- const
- let
- var

### Environnement par défaut

Un programme javascript aura un certain nombre de binding à disposition qui lui son fournit par son runtime (browser, on nodeJS), comme par exemple :

- `console` -> console.log("salut !")
- `alert` -> alert("Ceci est un message!")

Dans un navigateur, on peut ouvrir les devtools et obtenir la liste des bindings comme ceci : `console.log(Object.keys( window ));`

Dans un navigateur, il est a noter que la déclaration `var` ajoute un binding de niveau global affecté à l'object window (qui est lié à l'Iframe courant) :

```
Object.keys( window ).includes("toto")
 -> false
var toto = "salut";
 -> undefined
Object.keys( window ).includes("toto")
 -> true
```

### Control flow

#### If

```js
if (person.age > 20) {
  console.log("You seems adult")
} else {
  console.log("Soo young")
}
```


#### loops
​
```js
let number = 0;
while (number <= 12) {
  console.log(number);
  number = number + 2;
}
```

```js
let yourName;
do {
  yourName = prompt("Who are you?");
} while (!yourName);
```

```js
for (let i = 0; i < shellfish.length; i++) {
  console.log(i, shellfish[i]);
}
```

### Assignements courts

```js
let counter = 1;
counter += 1;
console.log(counter); // gives 2
```

### Commentaires

Comme en C / Java :
- single line `//`
- block `/* */`

```js

let toto = 3; // Single line comment

/*
Commentaire de plusieurs
lignes
*/

toto = 2;
```

## Functions basics

On peut déclarer la fonction de trois façons :

**Using declaration notation**
```js

console.log(`Square of 3 is ${square(3)}`)

function square(x) {
  return x * x;
}
```
**Using binding assignements**
```js
const square = function(x) {
  return x * x;
};
```

**Using arrow notation**
```js

const square = (x) => {
  return x * x;
};
```

Si il n'y a qu'un seul paramètre, on peut omettre () :

```js

const square = x => {
  return x * x;
};
```


On note :
- que la première forme ne se termine pas par un ;, les autres oui.
- que la première forme permet d'invoquer la fonction par du code précédant la déclaration.
- les fonctions peuvent être passée par valeur:

```js
function square(x) {
  return x * x;
}

let other_square_name = square;
console.log(`Square of 3 is ${other_square_name(3)}`);
```
- Qu'en pouvait passer les fonctions par valeur, une fonction peut **retourner** une fonction et ainsi donner lieu à de la programmation à base de **closure** :

```js
const any_power = (exposant) => {
  return (n) => {
    return n**exposant;
  };
}

let square = any_power(2);
console.log(`Square of 3 is ${square(3)}`);
```

**Nombre de paramètre aribitraire**

En utilisant la notation `...`:

```js
function max(...numbers) {
  let result = -Infinity;
  for (let number of numbers) {
      if (number > result) result = number;
    }
  return result;
}
```

**Immediately-invoked Function Expressions (IIFE)**

Peut se déclarer avec la arrow ou le mot clé function :

```js
(function() {
  /* */
})()

(() => {
  /* */
})()
```

Son use case son assez pointus, cf https://mariusschulz.com/blog/use-cases-for-javascripts-iifes

## JS datastructure : Objects basics

Toutes les structures de données non triviales en JS sont des objects.

Les objects sont une structures **clé-valeures**.

See https://www.digitalocean.com/community/tutorials/understanding-objects-in-javascript

On peut accéder à leurs **propriétés** ou leur **méthodes**, via l'usage du point ou du crocher :

```js
let mon_objet = new Object();
mon_objet.prop = "salut";
mon_objet.method = function(x) {
  return x * x
};

console.log(mon_objet.prop);
>> Salut
console.log(mon_objet["prop"]);
>> Salut
console.log(mon_objet.method(2))
>> 4
console.log(mon_objet["method"](2));
```

On peut avoir une déclaration alternative, qui utilise la notation accolades `{ }` ou `new Object()`;
```js
let mon_objet = {
  prop: "salut",
  method: (x) => { return x * x;}
};
```


On peut **effacer** une propriété d'un object, via le mot clé `delete`:
```js
let mon_objet = {
  prop: "salut",
  method: (x) => { return x * x;}
};

typeof(mon_objet.prop)
>> "string"
delete mon_objet.prop;
typeof(mon_objet.prop)
>> undefined
```

## JS Objects : Enumeration des clés/Valeurs

```js
const gimli = {
    name: "Gimli",
    race: "dwarf",
    weapon: "battle axe",
};

// Iterate through properties of gimli
for (let key in gimli) {
  console.log(gimli[key]);
}

>>>Gimli
>>>dwarf
>>>battle axe

// Get keys and values of gimli properties
for (let key in gimli) {
  console.log(key.toUpperCase() + ':', gimli[key]);
}

>>>NAME: Gimli
>>>RACE: dwarf
>>>WEAPON: battle axe

Object.keys(gimli);
>>> ["name", "race", "weapon"]

Object.values(gimli)
>>> [ "Gimli", "dwarf", "battle axe" ]
```

## JS Arrays

Il s'agit d'un type d'objet spécialisé pour stocker des données en tableau. Les propriété sont un index partant de 0, les valeurs ... le contenu du tableau.

On peut les déclarer via [] ou new Array.

**basics**

```js
let seaCreatures = [
    "octopus",
    "squid",
    "shark",
    "seahorse",
    "starfish",
];

seaCreatures[1];
>>> "octopus"

seaCreatures.indexOf("octopus");
>>> 1

seaCreatures.indexOf("unknown element");
>>> -1

seaCreatures.length;
>>> 5
```

**add items**

Via la manipulation d'index :

```js
let animals = [
    "cat",
    "dog"
];

animals[2] = "mouse";

animals;
>>> Array(3) [ "cat", "dog", "mouse" ]

animals[8] = "horse";
animals;

>>> Array(9) [ "cat", "dog", "mouse", undefined, undefined,
>>>            undefined, undefined, undefined, "horse" ]

```

Via `push()` / `unschift()` :
```js
let animals = [
    "cat",
    "dog"
];

animals.push("first");
animals.unshift("last");

animals;

>>> Array(4) [ "last", "cat", "dog", "first" ]
```

**remove items**
```js
let tableau = [
    "premier",
    "2eme",
    "3eme"
];

tableau.pop();
tableau;
>>> Array [ "premier", "2eme" ]

tableau.shift();
tableau;
>>> Array [ "2eme" ]
```

```js
let tableau = [
    "premier",
    "2eme",
    "3eme"
];

tableau.slice(1,2); //Note : retourne une copie, le tableau originanl n'est pas
                    //       modifié
>>> Array [ "2eme" ]
```

**Parcourir le tableau**

```js
let shellfish = [
    "oyster",
    "shrimp",
    "clam",
    "mussel",
];

// Parcourt du tableau ancien style (ne pas utiliser)
for (let i = 0; i < shellfish.length; i++) {
  console.log(i, shellfish[i]);
}

// Nouveau style (ES6)
for (let mammal of mammals) {
    console.log(mammal);
}
```

:warning: On note que la boucle de tableau est `for .. of` ... à ne pas confondre
avec la boucle d'object `for .. in` !


## Autres structures

See https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux

Map, Set, WeakMap, WeakSet, etc.

## Destructuring

On peut 'applatir' un objet durant l'assignation :

https://flaviocopes.com/javascript-destructuring/


## Sérialization (JSON)

On peut définir des objects imbriqué de complexité arbitraire et les serializer :

Par example, un tableau d'objets :

```js

let my_data = [
  {
      nom: "JF",
      age: 47
  },
  {
    nom: "Chloé",
    age: 12
  },
  {
    nom: "Yoan",
    age: 10
  }
];

let my_data_str = JSON.stringify(my_data);

console.log(my_data_str);
>>> [{"nom":"JF","age":47},{"nom":"Chloé","age":12},
>>>  {"nom":"Yoan","age":10}]

// On reconstruit un objet a partir de sa représentation
// texte JSON
console.log(JSON.parse(my_data_str));
>>> Array(3) [ {…}, {…}, {…} ]
```
