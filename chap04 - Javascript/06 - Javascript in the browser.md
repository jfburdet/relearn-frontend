# Javascript in the browser

## Déclaration et execution

https://www.digitalocean.com/community/tutorials/how-to-add-javascript-to-html

On peut déclarer une référence vers un fichier javascript, ou directement insérer du code :

```html
<script>
  alert("hello!");
</script>

<script src="js/script.js"></script>

```

On peut également directement le déclarer dans des éléments HTML sous forme de
appelé comme callback :

```html
<button onclick="alert('Boom!');">DO NOT PRESS</button>
```

La **position** de la déclaration javascript dans le fichier HTML peut-être à plusieurs endroit. Mais il
faut savoir que le code est exécuté dès que la déclaration est rencontrée, donc si la déclaration est
dans le HEAD, il faut se méfier car le code s'exécutera avant par exemple que le DOM (décrit plus bas dans la page)
n'ait été rendu par le browser.


```html
<!DOCTYPE html>
<html lang="en-US">

<head>
    <!-- Première possibilité -->
    <script src="js/script.js"></script>
</head>

<body>

</body>

<!-- Seconde possibilité -->
<script src="js/script.js"></script>

</html>
```

Il faut aussi noter que si il y a plusieurs tag script, dont un qui est un chargement distant, ce dernier sera exécuté après. D'où l'importance de se binder `window.onload` (cf juste après) pour s'assurer un démarrage déterministe et sans surprises.

### Comment éviter exécution prématurée

Le script de cette page va poser problème :

Click to [test](examples/ch2_ex1.html).

```html
<!DOCTYPE html>
<html lang="en-US">

<head>
    <script>
      let btn = document.getElementById("mon_bouton");

      // Cela cause une null pointer, car l'élément bouton n'existe pas encore.
      btn.innerText = "Was modified !"
    </script>
</head>

<body>
  <button id="mon_bouton">Click Me !</button>

    <p>
      Ouvre la devtools console (F12), pour constater une erreur 'btn is null'.
    </p>
</body>

</html>
```

**Solution1** : Déporter mon script en fin de page

Click to [test](examples/ch2_ex2.html).

```html
<!DOCTYPE html>
<html lang="en-US">

<head>  
</head>

<body>
  <button id="mon_bouton">Click Me !</button>
</body>

<script>
  let btn = document.getElementById("mon_bouton");

  btn.innerText = "Was modified !"
</script>

</html>
```

**Solution2** : Utiliser un event

Click to [test](examples/ch2_ex3.html).

Le modèle d'évènement du navigateur nous permet d'exécuter du code au moment précis ou le navigateur à fini le rendu des éléments HTML :

```html
<!DOCTYPE html>
<html lang="en-US">

<head>
  <script>

    window.onload = start;

    function start() {
      let btn = document.getElementById("mon_bouton");
      btn.innerText = "Was modified !";
    }

  </script>
</head>

<body>
  <button id="mon_bouton">Click Me !</button>
</body>

</html>
```

## Javascript and the DOM

https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model
https://developer.mozilla.org/en-US/docs/Web/API/Element

Le HTML contenu sur la page est rendu par le navigateur dans une structure arborescente XML standardisée, le DOM.
La hiérarchie des éléments est reproduite dans l'arbre DOM.

Ainsi, chaque élément peut avoir des attributs et une collection de noeuds enfants.

L'accès au DOM se fait via le mot clé `document` et des appels à ses enfants :

- `document.documentElement` -> élément partant de <html>
- `document.body` -> élément partant du <body>

Ensuite à partir d'un élément (pex elem), on peut naviguer :
- `elem.childNodes` : array ordonné de noeud enfants
- `elem.firstChild` : premier enfant
- `elem.lastChild` : dernier enfant
- `elem.parentNode` : element parent
- `elem.nextSibling` : element suivant au même niveau de l'arbre (un voisin)
- `elem.previousSibling` : voisin précédent

**texte** : Attention, la valeur interne de l'élément p.ex (`<p>Le texte</p>`) est définie et accessible via un noeud endant de type texte ou le champ `innerText` (ou `innerHTML`).

### Les attributs

Si on considère l'élément `<p data="secret" style="color:blue;font-size:46px;">Mon texte</p>`, et que elem point dessus, nous aurons :
- elem.text => 'Mon texte'
- element.getAttributes("data") => 'secret'
- element.attributes => NamedNodeMap(3) [ id="my_p", data="secret", style="color:blue;font-size:46px;" ]

Les éléments ont également beaucoup d'autres propriétés, see https://developer.mozilla.org/en-US/docs/Web/API/Element

**Attribut ID**

L'attribut ID est censé être unique au sein du DOM. Cependant en cas de doublon, le premier élément sera retourné sans erreur.

### Selection d'éléments

On peut faire un query global sur le DOM, qui retourne une collection ou un élément avec, selon si le nom de la méthod à un 's' sur element ou pas :

- **document.getElementById** : sélection par l'ID unique
- **document.getElementsByClassName** : Sélection par l'attribut de style `class`
- **document.getElementsByTagName** : Selection par le type d'élément (div)
- **document.getElementsByName** : Sélection par l'attribut `name`.
- **document.querySelector** : Sélection du premier élément satisfaisant le sélecteur css en paramètre : p.ex `var el = document.querySelector("div.user-panel.main input[name='login']");`
- **document.querySelectorAll** : (idem, mais retourne tous)

A noter que **querySelector** (et querySelectorAll) peuvent être utilisé sur un élément donné, afin de restreindre la recherche à ses enfants.

## Modifier le DOM

On peut créer des éléments DOM au vol pour peupler le HTML de la page de manière dynamique. C'est d'ailleurs ce que font par exemple VueJS :

```HTML
<p>One</p>
<p>Two</p>
<p>Three</p>
<script>
  let secondPar = document.body.getElementsByTagName("p")[1];
  let newPar = document.createElement("p");

  newPar.innerText = "Voici le nouveau paragraphe";
  newPar.style.color = "red";

  document.body.insertBefore(newPar, secondPar);
</script>
```

Méthode les plus usitées :
- replaceChild
- insertBefore
- createAttribute
- createTextNode
- createElement

## Les évènements du browser

Pour réagir aux inputs de l'utilisateur sur le browser, via son clavier ou sa souris, ou à divers évènements liés à la gestion de la page par le browser (p.ex "chargement terminé"), un système d'évènements permet à du code d'être appelé.

On attache/détache du code de callback à un évènement lié à un élément du DOM via :

```js
elem.add[|remove]EventListener("eventName", callback)
```

Le callback sera appelé avec passé en argument un objet décrivant l'évènement.

Exemples d'évènements :
- `click` : click de souris (appuyé + relevé) sur même élément.
- `mousdown` / `mouseup` : click de souris
- `keydown` / `keyup` : touche de clavier
- `blur` : déclenché quand on élément perd le focus
- `focus` : déclenché quand on obtient focus.
- `touchstart` / `touchmove` / `touchend` : Interaction avec écran tactile.
- `load` : La page a été chargée
- `beforeunload` : la page se ferme (une navigation vers autre page survient)

```js
<button>Click me</button>
<p>No handler here.</p>
<script>
  let button = document.querySelector("button");
  button.addEventListener("click", () => {
  console.log("Button clicked.");
  });
</script>
```

### Utilisation de l'event object

L'objet passé en paramètre du handler contient une foule d'information permettant de traiter
l'évènement, p.ex pour un event "click" :
- là ou on a cliqué (.x .y)
- le bouton de la souris (.button)
- si on avait les touches de clavier alt/shif/autres enfoncées (.altKey, .shiftKey, ...)

Ce qui permet d'utiliser l'event pex pour de l'animation : En cliquant sur la div orange et en draguant, on change sa taille :

```js
<p>Drag the bar to change its width:</p>
<div style="background: orange; width: 60px; height: 20px">
</div>
<script>
  let lastX; // Tracks the last observed mouse X position
  let bar = document.querySelector("div");
  bar.addEventListener("mousedown", event => {
    if (event.button == 0) {
      lastX = event.clientX;
      window.addEventListener("mousemove", moved);
      event.preventDefault(); // Prevent selection
    }
  });
  function moved(event) {
    if (event.buttons == 0) {
      window.removeEventListener("mousemove", moved);
    } else {
      let dist = event.clientX - lastX;
      let newWidth = Math.max(10, bar.offsetWidth + dist);
      bar.style.width = newWidth + "px";
      lastX = event.clientX;
    }
  }
</script>
```

Autre exemple, on capture le scroll, et on met à jour la largeur d'une div
qui reste fixe en haut, largeur représentant le pourcentage du scroll :

```html
<style>
#progress {
  border-bottom: 2px solid blue;
  width: 0;
  position: fixed;
  top: 0; left: 0;
}
</style>
<div id="progress"></div>
<script>
  // Create some content
  document.body.appendChild(document.createTextNode(
    "supercalifragilisticexpialidocious ".repeat(1000)));
  let bar = document.querySelector("#progress");
  window.addEventListener("scroll", () => {
    let max = document.body.scrollHeight - innerHeight;
    bar.style.width = `${(pageYOffset / max) * 100}%`;
  });
</script>
```

### Event propagation ("bubbling")

Quand des handlers sont instantiée sur le même event, mais sur une hiérarchie, le handler de l'élément le plus spécifique est d'abord
appelé, puis l'event "bubble" ( = remonte/ se propage) vers les parents.

Le handler de l'enfant à la possibilité de stopper la propagation et celui du parent de vérifier si le click a bien été fait sur lui ou sur l'enfant par exemple : Coller ce code dans codepen et ensuite commenter le stopPropagation

```html
<p>
Un paragraphe avec un bouton : <button>Click Me !</button>
<script>
  let para = document.querySelector("p");
  let btn = document.querySelector("button");
  para.addEventListener("click", (event) => {
    console.log(event.target.nodeName);
    if (event.target == para) {
      console.log("Para : Clicked on me");
    }
    if (event.target == btn) {
      console.log("Para : Clicked child button");
    }
  });
  btn.addEventListener("click", (event) => {
      console.log("Bouton : click ! (prevent bubbling)");
      event.stopPropagation();
  });
</script>
```

### Actions par défaut

En fonction de l'élément concerné, certains actions par défauts sont associées à l'event par le navigateur.
L'exemple le plus simple étant de naviguer quand un hyperlien est cliqué. L'example ci-dessous désactive tous les liens
de la page :

```html
<p>
  Voici un premier <a href="https://www.google.com">lien</a> suivit d'un <a href="https://www.unige.ch">second</a> qui sont
  normalement tous les deux désactivés.
  <script>
    let links = document.querySelectorAll("a");
    links.forEach( (lnk) => {
      lnk.addEventListener("click", (event) => {
      	console.log(`Tu n'ira pas ici ${event.target.href}`);
        event.preventDefault();
      });
    });
  </script>
</p>
```

### Timeouts / Timers

Rappel : `setTimeout` crée un callback appelé après un certains temps; `setInterval` fait la même chose, mais de manière répétitive. A leur instantiation, ils retournent un objet qui peut être utilisé pour le désactiver :

```js
let bombTimer = setTimeout(() => {
  console.log("BOOM!");
}, 500);
if (Math.random() < 0.5) { // 50% chance
  console.log("Defused.");
  clearTimeout(bombTimer);
}
```

### Debouncing events

Certains type d'évènement sont envoyé en raffale, p.ex :

```html
<div style="height:200px; width:200px;border:1px solid black;position: relative">Ma grande div</div>
<script>
	let div = document.querySelector("div");
    div.addEventListener("mousemove", event => {
      console.log("Event !")
    });
</script>
```

En console, on voit que le compteur d'occurence de "Event !" augmente vite ...

**On doit donc s'abstenir de faire du calcul intensif** pour ces events sous peine de rendre la page non-répondante.

Une solution est d'introduire un délai, via setTimeout :
- A chaque évènement, on définit un évènement courant s'il n'existe pas. S'il existe, on passe notre tour
- On agit sur cette évènement qu'après un timer
- Une fois timer exécuté, on indique que évènement courant a été traité.

```html
<script>
let eventToProcess = null;
window.addEventListener("mousemove", event => {
if (!eventToProcess ) {
  setTimeout(() => {
    document.body.textContent =
    `Mouse at ${eventToProcess.pageX}, ${eventToProcess.pageY}`;
    eventToProcess = null;
  }, 250);
}
eventToProcess = event;
});
</script>
```

A noter qu'on peut utiliser une fonction de la lib **lodash** pour réaliser la même chose :

```html
<html lang="en-US">
<body>

<script src="https://cdn.jsdelivr.net/npm/lodash@4.13.1/lodash.min.js"></script>
<script>

function debouncedMousemove(event) {
  document.body.textContent =
  `Mouse at ${event.pageX}, ${event.pageY}`;
}

window.addEventListener("mousemove", _.debounce(debouncedMousemove, 250, { 'maxWait': 250 }));

</script>
 </body>
 </html>

```
