import {au_cube} from "./libs/ma_lib";
import _ from 'lodash';


const start = () => {

    document.querySelector("button").addEventListener("click", (event) => {
       let input = document.querySelector("input");

       let p1 = document.getElementById("one");

       p1.innerText = au_cube(input.value);
    });

    document.getElementById("two").innerText = _.join(["salut", "mon", "ami", "!"], " ");


}



window.onload = start;