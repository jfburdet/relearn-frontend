const au_cube = (x) => {
    x = parseFloat(x);
    console.log(`cube(${x})`)
    return x ** 3;
}

const au_carre = (x) => {
    x = parseFloat(x);
    console.log(`carre(${x})`)
    return x ** 2;
}

const TWOPI = Math.PI * 2;

export {au_cube, TWOPI};