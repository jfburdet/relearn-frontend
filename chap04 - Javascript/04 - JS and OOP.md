# JS and OOP

Javascript a toujours été un language ou faire de l'orienté objet pouvait sembler un cauchemard.

Javascript réalise du OOP via le concept d'héritage de prototype.

https://flaviocopes.com/javascript-prototypal-inheritance/
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Details_of_the_Object_Model

Mais ça ... c'était avant ES6 qui a introduit la notation `class`.

## Javascript et héritage prototype

Quand on instantie un objet, ce dernier se voit attribuer une propriété qui pointe sur le prototype décrivant la classe de l'objet, et de ce fait hérite de tous

Cette façon de faire permet d'émuler de code OOP, avec l'instantiation d'une classe de l'hériage, de l'encapsultation, etc.

Le problème est que la syntaxe est laborieuse et peu lisible. Ceci est corrigé par la notation `class` introduite par ES6, qui bien que fonctionnant avec des prototypes, permet d'écrire les choses de manière plus claires.


Voici un exemple d'héritage avec l'ancienne syntaxe :

```js
function Hello(greeting) {
  this._greeting = greeting;
}

Hello.prototype.greeting = function () {
  return this._greeting;
};

function World() {
  Hello.call(this, 'hello');
}

// Copies the super prototype
World.prototype = Object.create(Hello.prototype);
// Makes constructor property reference the sub class
World.prototype.constructor = World;

World.prototype.worldGreeting = function () {
  const hello = Hello.prototype.greeting.call(this);
  return hello + ' world';
};

console.log(new World().greeting()); // Prints hello
console.log(new World().worldGreeting()); // Prints hello world

```

## Modern OOP (>ES6) in Javascipt

Maintenant l'exemple ci-dessous ré-écrit en syntaxe moderne :

```js
class Hello {
  constructor(greeting) {
    this._greeting = greeting;
  }

  greeting() {
    return this._greeting;
  }
}

class World extends Hello {
  constructor() {
    super('hello');
  }

  worldGreeting() {
    return super.greeting() + ' world';
  }
}

console.log(new World().greeting()); // Prints hello
console.log(new World().worldGreeting()); // Prints hello world
```


**Conclusion : Il faut utiliser la syntaxe moderne !**

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes

## More on modern OOP

### Méthode statique

```js
class Person {
  static genericHello() {
      return 'Hello';
  }
}

Person.genericHello();

// Attention, ci-dessous : cela ne marche pas !
let pers = new Person();
pers.genericHello(); // >>> TypeError: p.genericHello is not a function
```

### Getter and setter

```js
class Person {
  constructor(name) {
    this.name = name
  }
  set name(value) {
    this.name = value
  }
  get name() {
    return this.name
  }
}
```

### IsInstance

```js

class Person {

}

let toto = new Person();

console.log( toto instanceof Person); // -> true

let toto1 = new Object();

console.log( toto1 instanceof Person); // -> false
