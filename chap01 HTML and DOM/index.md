# HTML and DOM


Le HTML décrit le document affiché par le navigateur, le navigateur le convertit en un arbre d'objets qui a une racine, des noeuds enfants. Chaque noeud à un attribut.

For HTML, see :
- https://www.w3schools.com/html/html_intro.asp

For a DOM Intro, see :
- https://eloquentjavascript.net/14_dom.html

## HTML5 Semantic Elements

Afin de mieux structurer le document HTML et de rendre les choses plus lisibles, il est recommandé d'utiliser les nouveaux containers HTML5 à la place d'un tas de DIV :

https://www.w3schools.com/html/html5_semantic_elements.asp
