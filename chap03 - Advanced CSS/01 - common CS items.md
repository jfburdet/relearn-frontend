# Common CSS items

A mesure qu'on explore le CSS, divers autres éléments vont apparaître et avec lesquels il faut être familier.

## Les couleurs

Certaines propriété (p.ex color, background-color, border-color) acceptent des valeur exprimant des couleurs.

Les couleurs peuvent s'exprimer par :
- un **nom** (ou un code couleur), p.ex yellow, red : cf https://www.w3schools.com/cssref/css_colors.asp
```CSS
p {
    color: Lime;
}
```
- un code **hexa**
```CSS
p {
    color: #00ff00;
}
```
- avec le model **RGBa**
```CSS
p {
    color: rgb(0, 255, 0);
}
```
- avec le modèle **HSLa**
```CSS
p {
    color: hsl(120, 100%, 50%);
}
```

## Les unités

See and sandbox ("Try it" button) here : https://www.w3schools.com/cssref/css_units.asp

Utilisées pour déterminer les dimensions, elles peuvent être absolues (px, cm, mm, etc) ou relatives (%, em, vw, vh, etc.)

Certaines unités (p.ex cm, mm) ne sont pertinentes que pour des styles d'impression et non pas d'écran. (cf ci-dessous)

Example :
```css
.parent {
  width: 400px;
}
.child {
  width: 50%; /* = 200px */
}
```

#### Apparté sur l'unité em et rem

em se réfère à la taille calculée du parent, rem à celle de l'élément root.

See https://codepen.io/team/css-tricks/pen/a4b0f03f3fccdb8534cc6a3df6cf97ec

```HTML
<div class="container">
  <h2>This is a heading</h2>
  <p>This is some text.</p>
</div>
```

```css
.container {
  font-size: 16px;
}

p {
  font-size: 1em;
}

h2 {
  font-size: 2em;
}
```

## Les URL

Quand une valeur impliqueu d'aller charger une resource, on utilise le mot clé 'url'

```css
@import url(https://www.toto.ch/assets/file.css);
div {
    background-image: url(test.png);
}
div {
    background-image: url(subfolder/test.png);
}
div {
    background-image: url(https://mysite.com/test.png);
}
```



## type de media

Un sélecteur peut également s'appliquer que pour un affichage à l'écran ou impression, et même spécifier certaines contraintes comme la dimension du navigateur :
- https://www.w3schools.com/css/tryit.asp?filename=trycss3_media_queries1

On peut également le spécifier dans le header HTML

```HTML
<link rel="stylesheet" media="screen and (min-width: 900px)" href="widescreen.css">
<link rel="stylesheet" media="screen and (max-width: 600px)" href="smallscreen.css">
```

ou encore durant l'import (cf ci-dessous)

## L'inclusion / import de fichier

Chaque fichier CSS peut en importer d'autres. Attention, la directive d'import doit se trouver impérativement avant tout autre directive :
```css
@import url(myfile.css);
@import url(myfile.css) all;
@import url(myfile-screen.css) screen;
@import url(myfile-print.css) print;
```

## Calcul de dimensions

Avec le mot-clé "calc" on peut effectuer des calculs de dimensions dans les différentes unités

```css
div {
    max-width: calc(50% / 3)
}
div {
    max-width: calc(50% + 3px)
}
```

## Les commentaires

Le format est ```/* le commentaire */```

**Attention** à ne pas utiliser ```//``` car il en résultera une erreur de syntaxe souvent silencieuse.

```css
#name {
  display: block; /*
  color: red;
  */
}
```

## La gestion des polices de caractères

La spécification d'une police passe par plusieurs property (cf tableau di-dessous) qu'on peut agréger en une seule 'font' en respectant un ordre précis.

Pour la plupart des propriété réglables, leur disponibilité dépend de la 'font-familly' choisie. p.ex il se peut qu'une police n'ait pas toutes les variantes de gras (font-weight). Dans ce cas la valeur voisine sera affichée.

### font-familly

Il s'agit du nom commun de la police. Il faut la choisir de façon à ce que le navigateur la trouve dans ses polices par défaut, ou alors la charger d'internet (cf ```@font-face``` ci-dessous).

Cela dépendra également des frameworks CSS qu'on utilise (bootstrap, material).

```css
body {
  font-family: Helvetica, Arial;
}
```

### font-weight

Détermine le **niveau de gras** de la police :

- normal, bold
- bolder (relatif à l'élément parent)
- lighter (relatif à l'élément parent)
- 100, 200, 300 ... 900 : 400 = normal, 700 = bold

### font-strech

Détermine l'**étirement** ou la compression **latéral**.

Valeures possible, du plus écrasé ou plus étalé :

```
ultra-condensed
extra-condensed
condensed
semi-condensed
normal
semi-expanded
expanded
extra-expanded
ultra-expanded
```

### font-style (=> italic)

Une propriété (désuète ?) que permet seulement d'affecter une valeur indiquant si la police est obligue :
- normal
- italic (or oblique)

### font-size

Définit la taille générale de la police affichée.
On peut passer :
- Une dimension absolue : px, em, etc
- Un pourcentage : taille relative par rapport au parent
- un mot clé :
```
xx-small
x-small
small
medium
large
x-large
xx-large
smaller (relative to the parent element)
larger (relative to the parent element)
```

### font-variant (=> small-caps)

Permet de spécifier que les majuscule sont affichée en petit.

Valeures possible : normal, inherit, small


### Propriété 'font' tout-en-un

Les différentes propriété ci-dessus peuvent être mises explicitement

```css
body {
  font-family: Helvetica, Arial;
  font-variant: small;
}
```

ou alors, on peut spécifier la propriété font en alignant les sous-propriété selon :
```
font: <font-stretch> <font-style> <font-variant> <font-weight> <font-size> <line-height> <font-family>;
```

Exemple :
```css
body {
  font: italic bold 20px Helvetica;
}
section {
  font: small-caps bold 20px Helvetica;
}
```

### Téléchargement de polic @font-face

On peut ajouter des polices au naviateur via ce tag.

Attention, l'ajout de fonte doit se situer en premier dans les fichiers CSS.

Les types de police possibles sont :
- woff (Web Open Font Format)
- woff2  (Web Open Font Format 2.0)
- eot (Embedded Open Type)
- otf (OpenType Font)
- ttf (TrueType Font)

Exemple : see [font-face.html](examples/font-face.html)


## La typographie

La section des polices permettait de déterminer quelle police de caractère afficher, celle de la typographie, spécifie **comment le texte doit s'afficher dans son container**.

C'est un peu la partie "traitement de texte" : on peut définir la césure, la hauteur de ligne, comment formater la première ligne du paragraphe, l'alignement vertical, l'espacement entre les mots, etc.

Cf https://flaviocopes.com/css-typography/

## CSS Variables (ou "Custom properties")

Fonctionnalité récente de CSS, qui empiète un peu sur le domaine des pré-processeurs CSS.

La variable commence par **--** et peut être définie dans un sélecteur, qui indiquera à quel niveau du DOM elle devient disponible.

On l'utilisera via le mot-clé **var**.

Une variable pourra être manipulée par le Javascript.

Plus bas **:root** est un mot-clé désignant la racine du DOM.

Exemples :

```css
:root {
  --primary-color: yellow;
}

p {
  color: var(--primary-color)
}
```

```javascript
document.documentElement.style.setProperty('--primary-color', 'red')
```

See https://flaviocopes.com/css-variables/


## Gestion des background

Divers propriété permettent de gérer le fond d'écran affecté à un élément.

C'est assez basique, voir :
- https://flaviocopes.com/css-backgrounds/
- https://codepen.io/BernLeech/pen/mMNKJV
