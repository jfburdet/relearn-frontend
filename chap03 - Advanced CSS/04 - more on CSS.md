# More on CSS

Un peu en vrac, quelques notions pour terminer cette revue.

## Les tables

Façon classique de faire de la mise en page. Dorénavant on l'utilise QUE pour afficher des vraies tables :

```html
<table>
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Age</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Flavio</th>
      <td>36</td>
    </tr>
    <tr>
      <th scope="row">Roger</th>
      <td>7</td>
    </tr>
  </tbody>
</table>
```

```css
table, th, td {
  border: 1px solid #333;
  border-collapse: collapse; /* Fusionne le bord de table avec bord colonne */
}

tbody tr:nth-child(odd) {
  background-color: #af47ff;
}
```

A bricoler dans le codepen : https://codepen.io/jfburdet/pen/gOOwwRb

## Centrage

### Horizontal

En CSS moderne, on peut tout faire via le flex, y compris le text dans un <P> :

```css
p {
  display: flex;
  justify-content: center;
}
```

marche aussi bien que :
```css
p {
  text-align: center;
}
```

### Vertical

Historiquement difficile, maintenant on use le Flex :
```css
p {
  display: flex;
  align-items: center;
}
```

## Les listes

Mini rappel, il y a les unordered listes (ul), le préfix est un bullet :
```
<ul>
 <li>Coffee</li>
 <li>Tea</li>
 <li>Milk</li>
</ul>
```

et les ordered lists, le préfix est un numéro croissant:
```
<ol>
 <li>Coffee</li>
 <li>Tea</li>
 <li>Milk</li>
</ol>
```

ainsi que, moins utilisées, les descriptions list : une list d'éléments avec leur desription :

```
<dl>
 <dt>Coffee</dt>
 <dd>- black hot drink</dd>
 <dt>Milk</dt>
 <dd>- white cold drink</dd>
</dl>
```

Les propriété CSS qu'on peut utiliser sont :
- list-style-type : définit le type de bullet ou de numéro. Une tonne d'option disponible (cf lien plus bas)
- list-style-image : a la place d'un bullet on peut avoir une image (p.ex `list-style-image: url(list-image.png);`)
- list-style-position: [outside|inside] : Le numéro peut être affiché à l'exérieur du block délimitant l'élément.


See https://developer.mozilla.org/en-US/docs/Web/CSS/list-style-type


## Media queries et design responsif

L'idée est d'appliquer un jeu de règles css, soit en important des fichiers spécifique (`@import` CSS ou `link` html tag), soit en les définissant dans un bloc (`@media`) **en fonction des caractéristiques courante** de l'appareil qui affiche la page.

C'est le principe du **design responsif** : L'organisation de la page variera avec la taille et l'orientation de l'écran. P.ex les menu seront organisés différemment si la page est affichée sur un petit écran de téléphone en portrait que sur un écran de desktop 24'' en landscape, afin d'obtenir un affichage cohérent quel que soit la taille du dispositif qui affiche la page html.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Complete.png/190px-Complete.png)

See:
- https://www.w3schools.com/css/css_rwd_intro.asp
- https://developer.mozilla.org/en-US/docs/Web/CSS/@supports
- https://www.w3schools.com/css/css3_mediaqueries.asp
- https://www.w3schools.com/css/css3_mediaqueries_ex.asp
- https://codepen.io/ypmm/pen/pZMxYQ


Une alternative est d'utiliser l'**adaptative design** qui fournit une page html différente en fonction du user agent.

Les éléments utilisables sont :

### media-types
Définit le type de media sur lequel est rendu la page :
- `all` : tout media
- `print` : quand on utilise la fonction 'imprimmer' du navigateur
- `screen` : quand la page est présentée sur un écran
- `speech` : (rarement utilisé) Quand la page est rendue par un dispositif de lecture pour aveugle.

### media-features-descriptor

Ajoutés au media-type, permet de rendre plus spécifique la condition.

- width
- height
- device-width
- device-height
- aspect-ratio
- device-aspect-ratio
- color
- color-index
- monochrome
- resolution
- orientation
- scan
- grid

Ceux qui désignent une dimension, peuvent être préfixé par `max-` ou `min-`

### Exemples

```css
@import url(myfile.css) screen;
@import url(myfile-print.css) print;
```

```css
@import url(myfile.css) print, screen;
```

```html
<link rel="stylesheet" type="text/css" href="myfile.css" media="screen" />
<link rel="stylesheet" type="text/css" href="another.css" media="screen, print" />
```

```css
@import url(myfile.css) screen and (max-width: 800px);
```

```css
@import url(myfile.css) screen and (min-resolution: 100dpi);
```

```css
@media screen and (max-width: 800px) and (min-width: 600px) and (orientation: landscape) {
/* enter some CSS */
}
```

### Design responsif et viewport

Afin de définir correctement la valeur d'affichage au sein du navigateur, il **faut** définir l'élément suivant dans chaque page :

```html
<meta name="viewport" content="width=device-width, initial-scale=1.0">
```


## Feature queries

Permet d'appliquer une règle, seulement si le navigateur supporte la fonctionalité.

P.ex Le CSS suivant permet de définir du CSS qui s'applique QUE si le navigateur support la CSSGrid et FlexBox

```CSS
@supports (display: grid) and (display: flex) {
/* apply this CSS */
}
```


## Les filtres

Permet d'appliquer un filtre de transformation d'image style photshop, à un tag <img> **ainsi qu'a tout élément**.

p.ex
```css
img {
  filter: blur(4px);
}
```
Example :
- blur(4px) :
<img src="https://interactive-examples.mdn.mozilla.net/media/examples/firefox-logo.svg" width=100 style="filter: blur(4px)"/>
-  hue-rotate(90deg) <img src="https://interactive-examples.mdn.mozilla.net/media/examples/firefox-logo.svg" width=100 style="filter:  hue-rotate(90deg)"/>

On peut aussi filter via une image SVG pour obtenir effet spectaculaire : https://www.smashingmagazine.com/2015/05/why-the-svg-filter-is-awesome/

See https://developer.mozilla.org/fr/docs/Web/CSS/filter

## Les transformations

Permet d'opérer des transformation géométriques aux éléments affichés, soit sur le plan (2D) soit en produisant des effets de perspective en 3D. En fait il s'agit de projection linéaire de l'objet.

Les propriétés :
- translate (+ translateX/translateY)
- rotate (+ rotateX/rotateY)
- scale (+ scaleX/scaleY)
- skew (+ skewX/skewY)
- matrix
- perspective
- translateZ
- rotateZ
- scaleZ

p.ex

```css
.box {
  transform: scale(2, 0.5);
}
```

on peut aussi spécifier `transform-origin` pour déplacer le centre de rotation p.ex :


See https://codepen.io/vineethtr/full/XKKEgM


## Les transitions

Les transitions sont une forme d'animation basique. Elle permet de déterminer qu'un filtre CSS ne s'applique pas instantanément, mais que la modification est appliquée progressivement dans le temps. La fonction de transition temporelle peut être choisie parmis des presets ou définition via des paramètre de courbe de Bézier.

Les **dev tools** des navigateurs permettent de visualiser la transition et a un éditeur de fonction de bézier, très utiles pour explorer et trouver la transition qu'on veut.

## Les animations

Permet de faire des animations plus évoluées que les transitions, cela sans utiliser ni javascript, ni flash.

Une grande quantité de propriété est susceptible d'être animée.

On définit des étapes d'animations en utilisant le mot-clé `keyframe`, puis on assigne cette animation au éléments/container visés via la propriété `animation` :

```html
<div class="container">
  <div class="circle one"></div>
  <div class="circle two"></div>
  <div class="circle three"></div>
  <div class="circle four"></div>
</div>
```

```CSS
body {
	display: grid;
	place-items: center;
height: 100vh;
}

.circle {
	border-radius: 50%;
	left: calc(50% - 6.25em);
	top: calc(50% - 12.5em);
	transform-origin: 50% 12.5em;
	width: 12.5em;
	height: 12.5em;
	position: absolute;
	box-shadow: 0 1em 2em rgba(0, 0, 0, .5);
}

.one,
.three {
	background: rgba(142, 92, 205, .75);
}

.two,
.four {
	background: rgba(236, 252, 100, .75);
}

.one {
	transform: rotateZ(0);
}

.two {
	transform: rotateZ(90deg);
}See https://codepen.io/ypmm/pen/pZMxYQ

.three {
	transform: rotateZ(180deg);
}

.four {
	transform: rotateZ(-90deg);
}

@keyframes spin {
	0% {
		transform: rotateZ(0);
	}
	25% {
		transform: rotateZ(30deg);
	}
	50% {
		transform: rotateZ(270deg);
	}
	75% {
		transform: rotateZ(180deg);
	}
	100% {
		transform: rotateZ(360deg);
	}
}

.container {
  animation: spin 10s linear infinite;
}
```
See https://codepen.io/ypmm/pen/pZMxYQ
On peut enregistrer certains events en javascript :
- animationstarthttps://codepen.io/hurtado-nayza/full/bPxPVX
- animationend
- animationiteration

p.ex
```js
const container = document.querySelector('.container')
container.addEventListener('animationiteration', (e) => {
  //do something
}, false)
```

cf https://codepen.io/jfburdet/pen/rNNzOBG

See https://www.w3schools.com/css/css3_anihttps://codepen.io/hurtado-nayza/full/bPxPVXmations.asp
See https://flaviocopes.com/css-animations/


## Normalisation du CSS

Les styles de base disponible varie d'un browser à l'autre. Afin d'avoir une base commune, il convient d'utiliser un "normaliser" qui va enlever ces différence, régler des bugs et proposer une base propre.

P.ex http://necolas.github.io/normalize.css/

## Gestion des erreurs

Attention, les erreurs CSS sont souvent insidieuses et silencieuse : Il ne faut donc pas hésiter à utiliser un Linter pour les détecter le plus en amont possible.


## Les préfixes vendeurs

Syntaxe permettant au éditeur de navigateurs d'introduire des nouvelles features. Elles deviennent **obsolètes** et ne sont citées ici que pour référence afin de comprendre quand je tombe dessus en explorant le CSS de sites.

Les différents préfixes sont :
- -webkit- : Chrome, Safari, iOS Safari / iOS WebView, Android
- -moz- : Firefox
