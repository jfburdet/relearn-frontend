# Learning or reference resources pour CSS

- https://css-tricks.com
- https://caniuse.com
- https://flaviocopes.com/tags/css/
- https://www.w3schools.com/css/
- [The CSS Handbook](../chap02 CSS basics/The_CSS_Handbook_Flavio_Copes.pdf)
