# Les blocs CSS

## Le box model

Chaque élément ou bloc CSS est modélisé par une boite rectangulaire qui a :
- un contenu
- un padding (=espace à l'**intérieur** du bord)
- un bord
- une marge (=espace à l'**extérieur** du bord)

![](https://mdn.mozillademos.org/files/16558/box-model.png)

Pour visualiser et décourir les concepts, lancer un dev tools (F12) de Firefox sur p.ex https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/The_box_model#Playing_with_box_models

Dans le modèle par défaut, quand on modifie la hauteur ou largeur d'un élément, cela influence le **content area** (par opposition à la taille hors-tout, c.a.d. celle incluant les margins), puis le reste (padding, marging) est adapté en conséquence. Ce comportement peut être modifié via la propriété box-sizing.

Certains spécialistes recommande d'utilise l'autre mode : https://css-tricks.com/international-box-sizing-awareness-day/

Un bon résumé : https://codepen.io/suecerq/pen/dKBLgd

## Les bords

Divers attributs permettent de définir le bord du composant : https://flaviocopes.com/css-border/

For more advanced tricks, see https://css-tricks.com/almanac/properties/b/border-image/

## Padding

Défini l'espace intérieur au bord

On peut utiliser les quatre properties séparées ou les grouper. Les valeurs sont n'importe quel unité de [longueur](https://developer.mozilla.org/en-US/docs/Web/CSS/length).

P.ex

```css
/* Avec des propriété séparées */
p {
  padding-top: 20px;
  padding-right: 10px;
  padding-bottom: 3em;
  padding-left: 1em;
}

/* Avec une seule propriété et toutes les valeurs */
p {
  /* top-right-bottom-left. */
  padding: 20px 10px 3em 1em;
}

/* une seule valeur partielle */
p {
  /* Une seule valeur : s'applique a top/right/bottom/left */
  padding: 20px;
}

/* une seule valeur partielle */
p {
  /* s'applique a top/right/bottom/left */
  padding: 20px
}

/* Deux valeurs partielles */
p {
  /* 20px -> top/bottom 10px -> left/right */
  padding: 20px 10px;
}

/* Trois valeurs partielles */
p {
  /* 20px -> top 10px -> left/right 5px -> bottom */
  padding: 20px 10px 5px;
}
```

## Margin

Défini l'espace extérieur au bord.

La logique d'application des propriétés suivante est identique à padding :
- margin-top
- margin-right
- margin-bottom
- margin-left

## Box-mode : border-box vs content box (box-sizing)

Comme mentionné plus, le modèle par défaut pour la détermination de la taille d'un block (box) et le content-box, ce qui veut dire que les propriété de taille de l'élément, s'applique au "content", alors que dans le modèle alternatif **qu'il est recommandé d'utiliser**, la taille s'applique jusqu'au **bord** en laissant la marge à l'extérieur.

Pour l'appliquer à tous le document :

```css
*, *:before, *:after {
  box-sizing: border-box;
}
```

## :bangbang: La propriété "display" :bangbang:

Elle détermine comment un block (=une boîte) est affichée par le navigateur. Il y a beaucoup de valeur possible, mais dans le cadre du box-model, nous nous intéresserons dans un premier aux quatres valeurs suivantes :
- inline
- inline-block
- block
- none

Sandbox pour ces 4 propriétés : https://www.w3schools.com/cssref/tryit.asp?filename=trycss_display

Pour une explication claire : https://medium.com/better-programming/understanding-css-display-none-block-inline-and-inline-block-63f6510df93

### inline (défault)

Le block en mode inline s'affiche :
- en prenant juste l'espace nécessaire
- ne fait pas de retour de ligne (s'affiche à côté du précédent)
- ignore ses attributs de taille, padding, marge

### inline-block

Commen inline, mais les propriétés width et height sont quand même appliquée

### block

En mode block, qui est le mode par défaut de `div, p, section, ul`, l'élément s'affiche :
- En prenant 100% de la place en largeur dans son parent
- En étant empilé verticalement

### none

L'élément est invisible (mais présent dans le DOM) mais n'est pas affiché et ne prend pas de place.

### `display: none` vs. `visibility: hidden`

- `display: none` : L'élément reste dans le DOM, mais le browser fait comme s'il n'existait pas du tout.
- `visibility: hidden` : L'élément est invisible, mais l'espace qu'il prendrait reste présent ... mais vide.

## Positionnement

Permet de déterminer comme un élément est positionner à l'écran, et comment il occupe l'espace au sein de son parent.

C'est la propriété `position`, qui peut avoir les valeurs suivantes :
- static (default)
- relative
- absolute
- fixed
- sticky

Pour définir la position elle-même, on utilise les propriété
- top
- bottom
- left
- right

avec une unité de longueur.

### static (default)

C'est le mode par défaut, l'élément à sa place normale dans le flux de disposition de la page.

https://www.w3schools.com/css/tryit.asp?filename=trycss_position_static

### relative

On indique un décalage relatif à sa position "normale" (celle du flux normal de mise en page, celle de 'static'). Les autres éléments gardent leur position normale (comme si l'élément déplacé n'avait pas bougé)

https://www.w3schools.com/css/tryit.asp?filename=trycss_position_relative

### absolute

Permet de positionner par rapport au premier parent en remontant dans la hiérarchie qui est lui-même positionné ("être positionné" = attribut position != static). Si aucun élément parent n'est positionné, il le sera par rapport à l'élément `body`.

 :exclamation: Dans ce cas, l'élément est retiré du flot d'affichage normal (cf effet dans le codepen ci-dessous) comme s'il n'existait plus et ne prend donc plus sa place.

cf :
- https://codepen.io/flaviocopes/pen/WWGgrR mettre `position: absolute;` dans le style .box
- https://www.w3schools.com/css/tryit.asp?filename=trycss_position_absolute

### fixed

C'est exactement comme `absolute`, sauf que la référence est la zone globale d'affichage (le viewport)

cf : https://www.w3schools.com/css/tryit.asp?filename=trycss_position_fixed et essayer de changer la taille du browser.

### sticky

Permet de rendre un élément insensible au scroll, cf https://www.w3schools.com/css/tryit.asp?filename=trycss_position_sticky

## Floating and clearing

Permet de rendre un élément "flottant" au sein d'un autre. Un peu comme dans un traitement de texte, quand on insère une image dans un paragraphe, un peu définir comment est le flot de texte autour d'elle.

En fait, cela retire l'élément du flux d'affichage, pour le mettre "par dessus", flottant.

Fortement utilisée avant que CSS n'intègre des notions de mise en page plus naturelle **son usage osbcure et des fois difficilement prévisible est remplacé par des procécé de mise en page plus modernes.** comme CssGrid et FlexBox

### floating

Gère comment l'élément se positionne, la propriété `float` supporte trois valeurs :
- `left`
- `right`
- `none` (default)

See https://codepen.io/flaviocopes/pen/WWGqPr

et essayer de modifer le sélecteur img et modifier la valeur `right` par `left`.

### clearing

_Fonctionnement toujours pas très clair pour moi :-(_

Permet de dire que l'élément concerné n'est plus flottant, mais regagne le flux de rendu normal.

`left` veut dire : "L'élément concerne regarde le flux de rendu, va à la ligne et il ne doit avoir aucun élément flottant à sa gauche"

Avec le tag `clear`, nous pouvons changer ce comportement, il peut avoir les valeurs suivantes :
- left
- right
- both
- none (default)

Exemple :
- https://codepen.io/mastastealth/pen/xyeli (avancé)

## Z-Index

C'est un attribut qui définit quel composant est visible en cas de chevauchement.

Il ne marche que pour `position=[absolute|relative|fixed]` (pas pour static).

Il prend pour paramètre un entier : En cas de conflit, l'élément avec la plus grande valeur apparaît devant.

Un exemple pour expérimenter : https://codepen.io/Tipue/pen/pbEio/
