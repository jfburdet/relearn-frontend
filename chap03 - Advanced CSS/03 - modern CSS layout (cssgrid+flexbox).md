# Modern CSS Layout

Pour des raisons historiques, il a toujours été difficile du layout avancé en CSS, et nombre de bricolages (tables, float/clear, utilisation de `display: table`) étaient utilisés, donnant des résultats compliqués.

Cette période **est révolue**, car avec les nouvelles version de CSS, supportée par les navigateurs modernes, il existe deux nouveaux outils :

- La CSS Grid : Pour la mise en page en grille (2 dimensions). The new cool kid in town !
- Le CSS FlexBox : Pour le flux (1 dimension) d'éléments, en remplacement de float/clear.

## CSS Grid introduction

- On utilise un container (div) parent, on lui affecte `display: grid`
- On affecte ensuite des propriété :
  - Au container pour l'apparence générale
  - Aux éléments pour personaliser (p.ex 1 case occupe deux cases)

Les concepts de base sont très facile d'accès.

Pour un descriptif complet : https://css-tricks.com/snippets/css/complete-guide-grid/


### grid-template-columns / grid-template-rows

Définit le **nombre de colonnes et de lignes** ainsi que leur taille.

```css

/* Ici nous définissons une grid de 4 colonnes et 2 lignes

.container {
  display: grid;
  grid-template-columns: 200px 200px 200px 200px;
  grid-template-rows: 300px 300px;
}
```

On peut aussi laisser la dimension en `auto`, ce qui permet de laisser la grille varier.

```html
<div class="container">
  <div class="cell">Cell 1</div>
  <div class="cell">Cell 2</div>
  <div class="cell">Cell 3</div>
  <div class="cell">Cell 4</div>
  <div class="cell">Cell 5</div>
  <div class="cell">Cell 6</div>
</div>
```

```css
.container {
  display: grid;
  grid-template-columns: 200px 200px;
  grid-template-rows: 100px auto 100px;
  border-color: black;
  border-style: solid;
}

.cell {
 border-color: red;
 border-style: solid;
}
```

Voir mon code pen : https://codepen.io/jfburdet/pen/Exxgarr

### Espacement entre les éléments

Via les propriétés :
- grid-gap
- grid-column-gap
- grid-row-gap

qu'on affecte au container.

See https://codepen.io/jfburdet/pen/ZEEpGKP

### Etalement d'un élément

Il est possible de spécifier qu'un élément va se répartir sur plusieur cellules de la grille. Cela se fait via :
- grid-column-start
- grid-column-end
- grid-row-start
- grid-row-end

Ou les raccourcis :
- grid-column: début / fin;
- grid-row: début / fin;
- grid-column: début span count;
- grid-row: début span count;

See https://codepen.io/jfburdet/pen/vYYXORm

**Attention**: Le compte se fait via les ligne virtuelle délimitratrice des colonnes ou lignes : Donc si on veut les 3 premières colonne, il faut spéficier 1 à 3 ( On voit qu'il y a trois traits : |   col1 |  col2  |)

### D'autres façon de spécifier les dimensions

#### unités variées

```css
.container {
  grid-template-columns: 3rem 15% 1fr 2fr;
}
```

Spécifie une grille de 4 colonnes :
- La 1ère : équivalent 30px (si zoom accessibilité 100%)
- La 2ème : 15% de l'espace en largeur disponible
- La 3ème : 1/3 de l'espace disponible des colonnes 3+4
- La 4ème : 2/3 de l'espace disponible des colonnes 3+4

Unité de fraction : attention, quand on a un mixte d'unité, la fraction concerne les éléments qui ont eux aussi la fraction, ce n'est pas la fraction du tout, mais la fraction des éléments qui ont l'unité fraction.

see https://codepen.io/jfburdet/pen/dyypYzd

Avec l'unité fraction on définit facilement les proportion d'une grille qui doit prendre tout la largeur de la page :
```css
.container {
  display: grid;
  height: 100vh;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-template-rows: 1fr 1fr;
}
```

See  https://codepen.io/jfburdet/pen/WNNGQYb

#### Raccourcis `repeat`

Permet de spécifier un nombre de colonne/ligne, plutôt que de les énumérer :
```css
.container {
  grid-template-columns: repeat(4, 100px);
}
```

ce qui est équivalent à :
```css
.container {
  grid-template-columns: 100px 100px 100px 100px;
}
```

### minmax

Permet de délimiter une taille max/min. P.ex
```css
.container {
  grid-template-columns: minmax(100px, auto) 100px;
}
```

### Positionnement par template : ```grid-template-areas```

Permet de définir l'agencement/chevauchement de manière 'visuelle' et intuitive.

Note: une cellule vide dans le template se définit via un point "."

p.ex

```HTML
<div class="container">
<main>
...
</main>
<aside>
...
</aside>
<header>
...
</header>
<footer>
...
</footer>
</div>
```

```css
.container {
display: grid;
  grid-template-columns: 200px 200px 200px 200px;
  grid-template-rows: 300px 300px;
  grid-template-areas:
  "header header header header"
  "sidebar main main main"
  "footer footer footer footer";
}
main {
  grid-area: main;
}
aside {
  grid-area: sidebar;
}
header {
  grid-area: header;
}
footer {
  grid-area: footer;
}
```

### Rendu conditionnel

On peut changer l'agencement quand la largeur de l'écran passe sous un certain seul, p.ex ici on met le side bar en bas plutôt que sur le côté, quand le viewport devient inférieur à 500px :

```css
@media (max-width: 500px) {
  .wrapper {
    grid-template-columns: 4fr;
    grid-template-areas:
    "header"
    "content"
    "sidebar"
    "footer";
  }
}
```

See https://codepen.io/flaviocopes/pen/JZWOEK  (essayer de réduire la taille horizontale du browser)

## The FlexBox

Permet de définir les règles de remplissage d'un container via l'agencement du flux (en ligne ou en colonne, mais pas les deux à la fois) de ses enfants. P.ex le flux de mots dans un <P>.

Pour activer le mode, on applique :
- `display:flex` : dans ce cas la le container sera affiché en mode "block"
- `display: inline-flex;` : le container lui-même sera inline et susceptible de suivre le flux courant et donc d'être alignés à la queue-leuleu.

### Les règles de containers

Elle s'applique au container qui a le display:flex, et gère le rendu global.
- flex-direction : direction du flux
- justify-content : type de justification
- align-items : alignement vertical
- flex-wrap : retour à la ligne
- flex-flow : (raccourci flex-direction+flex-wrap)

#### flex-direction

Détermine le type (ligne vs colonne) et le sens du flux.

```css
.container {
  flex-direction: [row|row-reverse|column|column-reverse]
}
```

p.ex
```html
<div class="container">
  <p>Para 1</p>
  <p>Para 2</p>
  <p>Para 3</p>
</div>
```
```css
.container {
  display: flex;
  flex-direction: row-reverse;
}
```

#### justify-content

Détermine comment le contenu est réparti :
- flex-start : s'aligne sur le début du flux (gauche normal, droite si reverse)
- flex-end : s'aligne sur la fin du flux (à droit si normal, gauche si reverse)
- center
- space-between : justifié avec espace, mais 1er et dernier élément collés au bords.
- space-around : justifié, avec des espaces.

See https://codepen.io/machal/pen/doGjaZ

#### align-items

Déterminer l'alignement vertical.

Valeurs possibles :
- flex-start : aligné en haut
- flex-end : aligné en bas
- center
- baseline: https://jsfiddle.net/hyvmnkd4/
- stretch: étendu pour occuper le container

Note : La valeur baseline semble un peu tricky : à creuser le cas échéant.

#### Wrapping

Permet de laisser les éléments faire un saut de ligne.

```css
.container {
  flex-wrap: [wrap|wrap-reverse]
}
```

See https://css-tricks.com/almanac/properties/f/flex-wrap/

### Les règles sur les éléments enfants

On peut également définir des règles sur les éléments enfants membre du container, pour personnaliser la façon dont ils apparaissent dans le flux.

- order : défini position
- align-self : alignement vertical
- flex-grow : facteur d'expansion
- flex-shrink : facteur d'écrasement
- flex-basis : court-circuite grow/shrink pour cet élément.
- flex : raccourci flex-grow+flex-shrink+flex-basis

See :
- https://codepen.io/raphaelgoetter/pen/bdYQML
- https://www.madebymike.com.au/demos/flexbox-tester/
