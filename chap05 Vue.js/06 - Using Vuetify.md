= Using Vuetify

Permet d'avoir :
- Un design attractif
- Une UIX optimale

Vuetify => Utilise les google material guidelines

L'unité d'information de base est une `card`

exemple, un module de login :

```html

<template>
  <v-app>
    <v-card width="400" class="mx-auto mt-5">
      <v-card-title class="pb-0">
        <h1>Login</h1>
      </v-card-title>
      <v-card-text>
        <v-form>
          <v-text-field
            label="Username"
            prepend-icon="mdi-account-circle"
          />
          <v-text-field
            :type="showPassword ? 'text' : 'password'"
            label="Password"
            prepend-icon="mdi-lock"
            :append-icon="showPassword ? 'mdi-eye' : 'mdi-eye-off'"
            @click:append="showPassword = !showPassword"
          />
        </v-form>
      </v-card-text>
      <v-divider></v-divider>
      <v-card-actions>
        <v-btn color="success">Register</v-btn>
        <v-btn color="info">Login</v-btn>
      </v-card-actions>
    </v-card>
  </v-app>
</template>

<script>
export default {
  name: 'App',
  data () {
    return {
      showPassword: false
    }
  }
}
</script>

```

Le résultat :

![](https://i.imgur.com/i2Ks2PG.png)

A noter :
- Les composant sont préfixés par `v-`
- L'unité d'info de base est vcard, qui contient des infos et des actions.
- Le composant inclu l'aspect visuel et ux

# Les composants standards

Pour consulter la liste des composants disponibles : https://vuetifyjs.com/en/components/api-explorer

Plusieurs éléments sont abordés :
- v-app : élément racine
- v-card : élément unitaire d'affichage dans le material guidelines.
- Le composant snackbar : qui affiche un message ephémère en bas de l'écran.
- v-app-bar : bar de menu principale
- v-footer


# Mise en page : Le système de grille vuetify

La mise en page de la grille est réalisée avec une grille basée sur `flex-box` avec 12 colonnes implicites.

La structure de base est :
```
v-container
   v-row
     v-col
```

Le composant `s-spacer` permet d'occuper l'espace blanc.

# Mise en page : Le design responsif

https://www.vuemastery.com/courses/beautify-with-vuetify/layouts-responsive-design

Différent codes **breakpoints** sont utilisées pour définir ce qu'on veut faire selon la dimension de l'affichage du navigateur :
- xs : extra-small : téléphone mobile
- sm : small : téléphone mobile en paysage ou petite tablette
- md : medium : Tablette grande ou petit notebook
- lg : large : desktop
- xl : desktop, écran 4k ou plus.

```html
<template>
  <v-container>
    <h1>Dashboard</h1>

    <v-row>
      <v-col v-for="sale in sales" :key="`${sale.title}`" cols="12" md="4">
        <SalesGraph :sale="sale" />
      </v-col>
    </v-row>

    <v-row>
      <v-col
        v-for="statistic in statistics"
        :key="`${statistic.title}`"
      >
        <StatisticCard :statistic="statistic" />
      </v-col>
    </v-row>

    <v-row>
      <v-col cols="8">
        <EmployeesTable :employees="employees" @select-employee="setEmployee" />
      </v-col>
      <v-col cols="4">
        <EventTimeline :timeline="timeline" />
      </v-col>
    </v-row>

    <v-snackbar v-model="snackbar" :left="$vuetify.breakpoint.lgAndUp">
      You have selected {{ selectedEmployee.name }},
      {{ selectedEmployee.title }}
      <v-btn color="pink" text @click="snackbar = false">
        Close
      </v-btn>
    </v-snackbar>
  </v-container>
</template>
```

Pour le code ci-dessus, on note :
- Qu'on doit avoir une structure `v-container` -> `v-row` -> `v-col`
- Que p.ex pour le SalesGraph, il doit prendre 4 colonnes (sur 12) en mode "md", sinon (p.ex sur un téléphone) il prend toute la largeur (12 sur 12)
- Que la snackbar sera à gauche, seulement pour les affichages "lg" et plus grands.

# Gestion des forms

En HTML simple, on utilise `<form></form>`, en vuetify : `<v-form></v-form>`

Avec vuetify, les éléments input et label habituel, sont condensé en un seul `v-text-field` :

```html
<v-text-field label="Name" type="text" />
```

Le forme peut contenir divers champs : https://vuetifyjs.com/en/components/categories/forms

Ainsi qu'un file upload : https://vuetifyjs.com/en/components/file-inputs#file-inputs

A noter qu'il existe également un composant vue standard évolue pour le file upload, qui support également le drag-drop et le paste : https://github.com/lian-yue/vue-upload-component/issues/122

# Validation de forms

Avec le système vuetify, on peut simplement attacher un array de fonction de validation qui sont exécutées l'une après l'autre. A noter qu'on peut utiliser d'autres systèmes de validations. See https://vuetifyjs.com/en/components/forms et les examples.
