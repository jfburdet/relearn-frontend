# VueJS Core concepts

## Introduction

Vue est un framework de rendu javascript qui permet de composer des applications web
a partir de composant (fichiers .vue) indépendants.

Les composants sont réactifs, c'est à dire que lorsque la donnée à laquelle ils sont liées change, il se met
automatiquement à jour.

Les composants .vue sont divisés en trois sections au sein du même fichier:
- `<template>` -> Qui contient les tag HTML
- `<script>`   -> Qui contient le code
- `<style>`    -> Qui contient le CSS


See :
- [Vue essential cheat sheet](./resources/Vue-Essentials-Cheat-Sheet.pdf)
- https://www.vuemastery.com/courses/intro-to-vue-js



## Utilisation

On peut soit l'utiliser directement via un CDN au sein d'un fichier html, pex pour du
prototyping ou de l'intégration partielle, soit utiliser le `vue-cli` et les yarn + module npm
qui permet de scafolder une application complète.

Pour apprendre les concepts de base, la **première** option est plus simple pour présenter des exemples concis qui suivent.

Pour tester les exemples ci-dessous, on peut simplement coller la partie qui est inclue dans body dans le champ de gauche sur https://codepen.io

Nous opterons pour les composants par la suite, lorsque nous aborderons des points plus évolués.


## Instantiation de l'app, interpolation et réactivité

```html
<!DOCTYPE html>
<html lang="en-US">
<head>
</head>

<body>
    <div id="my_app">
      Le montant vaut {{ montant }}. Et mon nom est {{ nom }}.
    </div>

   <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
   <script>
        let app = new Vue({
            el: "#my_app",
            data: {
              montant: 125,
              nom: "JF Burdet"
            }
        });
   </script>
</bod>
</html>
```

A noter :
- Nous utilisons le mode "CDN"
- Le point d'entrée de l'application est la création de l'objet "Vue" en lui passant un objet d'options qui :
  - Indique quel est l'élément racine dans le DOM où il doit se binder.
  - Spécifie ses données
- On voit que dans le HTML, on peut utiliser une **expression vue** entre crochet est interpolée avec la valeur liée dans data.
- Le contenu affiché par cette expression est liée et **réactif** avec l'objet javascript data : Ouvrir cette exemple dans un navigateur, utiliser les dev tools, et faire en console `app.montant = 10` -> On voit que l'affichage est mis à jour.
- Dans les dev tools, on peut aussi utiliser l'addons "vue" pour avoir un affichage direct de l'application.

On peut noter que `new Vue` supporterait aussi d'être appelée comme ceci :
```js
let app = new Vue({
    data: {
      montant: 125,
      nom: "JF Burdet"
    }
}).$mount("#my_app");
```

See :
- https://vuejs.org/v2/guide/instance.html


## Expressions vue

Une expression, qui se met en double accolade `{{ }}` peut utiliser tous les constructeur javascript, mais doit être **une expression unique** !

On peut utiliser des fonctions globale de javascript, mais pour utiliser nos propres fonctions il faudra les déclarer aussi dans l'objet options passé à `new Vue` (cf computed properties)

- `{{ nom.split("").reverse().join("") }}`  :+1:
- `{{ isworking ? 'YES' : 'NO' }}` :+1:
- `{{ nom = nom + " !"; nom.split("").reverse().join("") }}` :x:

## Interpolation d'attributs

Pour lié une donnée réactive à un attribut, on ne peut PAS utiliser `{{ }}`, on doit utiliser `v-bind:` :

```html
<html lang="en-US">
<body>

	<div  id="my_app">
	    <img v-bind:src="imgurl" alt="Une image" />
	</div>


   <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
   <script>
        let app = new Vue({
            el: "#my_app",
            data: {
              imgurl: "https://picsum.photos/200/300"
            }
        });
   </script>
</body>
</html>
```

**raccourcis** : v-bind étant tellement commun, qu'on peut l'abréger : ces deux écritures sont équivalentes :

```js
<img v-bind:src="imgurl" />
<img :src="imgurl" />
```

## Rendu conditionnel

https://vuejs.org/v2/guide/conditional.html

Un élément DOM peut apparaître, en fonction d'un état des données.

A noter :
- il peut être présent/absent du DOM avec `v-if`
- il peut être visible ou non avec `v-show` (css property : display:none)

Si le DOM est complexe, la seconde solution est plus efficace.

A faire:
- essayer les deux version avec les dev tools :
  - explorer
  - faire passer le stockCount à 0
  - explorer

v-if et v-show prennent en paramètre une expression, mais sans accolade.

Version 1 : avec v-if
```html
<html lang="en-US">
<body>

  <div  id="my_app">
    <p v-if="stockCount > 0">En stock ({{stockCount}})</p>
    <p v-else>Plus de stock</p>
  </div>

   <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
   <script>
        let app = new Vue({
            el: "#my_app",
            data: {
              stockCount: 10
            }
        });
   </script><html lang="en-US">
<body>

	<div  id="my_app">
	    <img
	    	v-bind:src="imgurl"
	    	@mouseover.once="set_url"
	    	alt="Passe la souris sur ce texte !" />
	</div>


   <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
   <script>
        let app = new Vue({
            el: "#my_app",
            data: {
              imgurl: ""
            },
            methods: {
            	set_url: function() {
            		this.imgurl = "https://picsum.photos/200/300";
            	}
            }
        });
   </script>
</body>
</html>
</body>
</html>
```

Version 2 : avec v-show
```html
<html lang="en-US">
<body>

  <div  id="my_app">
    <p v-show="stockCount > 0">En stock ({{stockCount}})</p>
    <p v-show="stockCount == 0">Plus de stock</p>
  </div>


   <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
   <script>
        let app = new Vue({
            el: "#my_app",
            data: {
              stockCount: 10
            }
        });
   </script>
</body>
</html>
```


Ou alors, pour grouper plusieurs éléments, sans utiliser un élément racine, on peut faire un v-if sur un objet html 'template' : en cas de rendu, le `<template>` sera remplacé par ses enfants.

```html
<template v-if="ok">
  <h1>Title</h1>
  <p>Paragraph 1</p>
  <p>Paragraph 2</p>
</template>
```

## Rendu de liste / boucle `v-for`

On peut boucler sur une liste d'élément pour réaliser le rendu d'une array de valeurs :

```html
<html lang="en-US">
<body>

  <div  id="my_app">

  	<p>Avec une liste:</p>

    <ul>
      <li v-for="item in itemCollection" :key="item.id">
        {{ item.name }}
      </li>
	</ul>

    <p>Avec des DIV:</p>

    <div v-for="item in itemCollection" :key="item.id">
    	{{ item.name }}
    </div>

  </div>


   <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
   <script>
        let app = new Vue({
            el: "#my_app",
            data: {
              itemCollection: [
                {id:1, name:"JF"},
                {id:2, name:"Céline"},
                {id:3, name:"Yoan"}
              ]
            }
        });
   </script>
</body>
</html>
```

**note** : L'usage de l'attribut `:key` est optionnel mais très recommandé si par la suite on veut identifier correctement un objet p.ex sélectionné par un click.

## Gestion des events DOM

On affecte à l'élément DOM la directive `v-on:` ou son abréviation `@`, avec pour argument, une expression qui sera évaluée lorsque l'évènement survient.

Si l'expression ne suffit pas à ce qu'on veut faire, on peut alors déclarer une fonction dans la section **methods** de l'objet de configuration.

Pour une description claire see https://vuejs.org/v2/guide/events.html, pariculièrement pour décrire les 'modifiers' sur les events.

Voici deux examples qui parlent d'eux même :
```HTML
<html lang="en-US">
<body>
  <div  id="my_app">
  		<button @click="ajoute_simple">Ajoute simple</button>
  		<button @click="ajoute($event, 2)">Ajoute deux (record click location)</button>

  		<p>Le total est {{ total }}</p>
  		<p>Le click a eu lieu {{ `${x}/${y}` }}</p>
  </div>

   <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
   <script>
        let app = new Vue({
            el: "#my_app",
            data: {
              total : 0,
              x : 0,
              y : 0
            },
            methods: {
            	ajoute_simple: function() {
            		this.total = this.total + 1;
            	},
            	ajoute: function(event, val) {
            		this.total = this.total + val;
            		this.x = event.x;
            		this.y = event.y;
            	}
            }
        });
   </script>
</body>
</html>
```

```HTML
<html lang="en-US">
<body>
	<div  id="my_app">
	    <img
	    	v-bind:src="imgurl"
	    	@mouseover.once="set_url"
	    	alt="Passe la souris sur ce texte !" />
	</div>

   <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
   <script>
        let app = new Vue({
            el: "#my_app",
            data: {
              imgurl: ""
            },
            methods: {
            	set_url: function() {
            		this.imgurl = "https://picsum.photos/200/300";
            	}
            }
        });
   </script>
</body>
</html>
```

## Remarques sur la déclaration de `methods`

Nous avons vu l'objet d'options passé au constructeur Vue peut contenir une options methods :
```js
let app = new Vue({
    el: "#my_app",
    data: { total : 0 },
    methods: {
      ajoute_simple: function() {
        this.total = this.total + 1;
      }
    }
});
```

On pourrait être tenté d'utiliser des closures ES6 moderne pour écrire :

```js
let app = new Vue({
    el: "#my_app",
    data: { total : 0 },
    methods: {
      ajoute_simple: () => {
        this.total = this.total + 1;
      }
    }
});
```

Mais **cela ne fonctionne pas** ... car avec ce type de closure, le scope de `this` change et n'est plus l'instance Vue mais l'objet Window. Donc `this.total` ne marche plus.

Ceci est illustré par cet exemple :

```HTML
<html lang="en-US">
<body>

  <p>Open dev tools console, then hit buttons</p>

  <div  id="my_app">
  		<button @click="normal">Methode function</button>
  		<button @click="arrow">Methode arrow function</button>
  </div>

   <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
   <script>
        let app = new Vue({
            el: "#my_app",
            methods: {
            	normal: function() {
            		console.log("Normal function : ");
            		console.log(" -> constructor : " + this.constructor)
            		console.log(" -> this " + this);
            	},
            	arrow: () => {
            		console.log("Arrow function : ");
					console.log(" -> constructor : " + this.constructor)
            		console.log(" -> this " + this);            	}
            }
        });
   </script>
</body>
</html>
```

## Inline-style binding

Bien que la bonne pratique voudrait qu'on utilise du CSS, on peut attacher un attribut de style en inline à n'importe quel élément HTML.

On peut modifier une propriété a la fois, ou utiliser un ou plusieurs objet encapsulant ceci.

https://vuejs.org/v2/guide/class-and-style.html#Binding-Inline-Styles

```html
<html lang="en-US">
<body>

  <p>Open dev tools console, then hit buttons</p>

  <div  id="my_app">
  		<button @click="click">Click me !</button>

  		<div :style="divStyle">
  			<p :style="{ 'color': pColor }">Mon texte</p>
  		</div>
  </div>

   <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
   <script>
        let app = new Vue({
            el: "#my_app",
            data : {
            	pColor : "black",
            	divStyle : {
            		'width': '40px',
  					'height': '40px',
  					'margin-top': '5px',
  					'background-color': 'green'
            	}
            },
            methods: {
            	click: function() {
            		this.pColor = "red";
            		this.divStyle.width = "80px";
            		this.divStyle.height = "80px";
            		this.divStyle["margin-top"] = "10px";
            		this.divStyle["background-color"] = "blue";
            	}
            }
        });
   </script>
</body>
</html>
```

## Class binding

On peut également dynamiquement définir les classes CSS affectées à un élément.

https://vuejs.org/v2/guide/class-and-style.html#Binding-HTML-Classes

pex

```html
<div
    class="static"
    v-bind:class="{ active: isActive, 'text-danger': hasError }"
</div>
<script>
// [...]
data: {
  isActive: true,
  hasError: false
}
</script>
```

Cela va donner : `<div class="static active"></div>`

Rappel : ci-dessus le `v-bind:class=...` peut s'écrire `:class=...`

Note: Tout comme le binding sur `style` on peut passer des classes séparée ou un/plusieurs object de classe (see doc above)

Exemple plus complet :

```html
<html lang="en-US">
  <head>
    <style>
      .stricked {
      	text-decoration: line-through;
      	color: red;
      }
    </style>
  </head>
<body>

  <div  id="my_app">
  		<button @click="click">Click me to decrease stock !</button>

  		<p> Stock count {{ stockCount }}.

		<p :class="{ 'stricked': this.stockCount == 0 }">En stock !</p>
  		</div>
  </div>

   <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
   <script>
        let app = new Vue({
            el: "#my_app",
            data : {
            	stockCount : 3
            },
            methods: {
            	click: function() {
            		if (this.stockCount>0) {
            			this.stockCount--;
            		}
            	}
            }
        });
   </script>
</body>
</html>
```

## Computed properties

Quand on veut faire de l'interpolation avec `{{ expression }}` et que la logique devient trop complexe pour tenir dans une seule
expression ... logique d'ailleurs qu'on ne veut pas dans le template, mais plutot dans le code, on utilise les *computed properties* :

```html
<html lang="en-US">
<body>

  <div  id="my_app">

    <button @click="click">Click me to decrease stock !</button>

    <p> Stock count {{ stockCount }}.

    <p v-if="showInStock">En stock</p>
    <p v-else>Plus de stock</p>
  </div>

   <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
   <script>
        let app = new Vue({
            el: "#my_app",
            data: {
              stockCount: 200
            },
            methods: {
              click: function() {
                if (this.stockCount>0) {
                  this.stockCount--;
                }
              }
            },
            computed: {
              showInStock: function() {
                return this.stockCount % 2 == 0 && Math.random()>0.5;
              }
            }
        });
   </script>
 </body>
 </html>
```

**Attention** : Les computed properties ont un système de cache : elles ne se mettent à jour QUE si ses composant réactifs (ici this.stockCount) ont changé. Cela permet de limiter les mises à jour dans l'UI.

Si on ne veut pas du système de cache, on aurait pu utiliser une méthode normale ... qui aurait été appelée à chaque re-rendu du composant.

See also https://codepen.io/GreggPollack/pen/KJOzoQ?editors=1011

## Watched properties

On peut faire un réglage plus fin que les computed propoerties, avec des watched properties.

Plus de détails : https://vuejs.org/v2/guide/computed.html#Watchers
