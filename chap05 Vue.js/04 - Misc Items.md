# Progress bar

On va utiliser nprogress.

Trois méthodes :

- Axios interceptor ... easy, mais quand il y a plusieurs requête ca peut coincer
- Axios interceptor in Vuex : ok, mais du coup, on voit le template vide du composant avant qu'il se remplisse.

-> Implémenté basiquement dans l'apps pour l'instant. A refactorer ensuite.


# Axios interceptor

A utiliser pour :

- Injecter un token d'authentification dans nos requête
- Pour intercepter des erreurs 'unauthorized' et rediriger vers le login.

# Gestion des évènements/notifications

On peut facilement prévoir un lieu central pour les notifications et les afficher sous forme de div qui s'affichent
sur le coté puis disparaissent après un délai ou en cliquant dessus.

# Composant réutilisable

Nous montre comment écrire des composants standards.

Ca parle d'héritage d'attributs : mais attention en vue2.0, ceux lié au style ne s'héritent pas de la même façon que les autres.

De plus y'a un syntaxique sugard a revoir et un système d'event enfant->parent, car je l'ai mal compris.

See :
- https://www.vuemastery.com/courses/next-level-vue/reusable-form-components-baseinput (scroll to middle, et video entre 2mn -> 5mn10, résumé visuel a 4mn30)
- https://vuejs.org/v2/guide/components.html#Using-v-model-on-Components


# Form validation

Il s'agit de vérifier qu'une forme est remplie avec les bon éléments.

-> `yarn add vuelidate` : c'est la lib qu'on va utiliser.

https://vuelidate.js.org/#getting-started

Il existe également une autre lib : https://logaretm.github.io/vee-validate/

# Mixins

Les mixins sont un pattern qui permet d'avoir du code partagé entre deux composants, c.a.d de grouper du code commun à plusieurs composant au sein d'un même mixin.

(**à confirmer** : Ils semble qu'avec la composition api de vue3, ils deviennent inutiles)

https://vuejs.org/v2/guide/mixins.html

En cas de conflit de nommage, la règle par défaut est que l'élément dans le composant prend le dessus.

On peut avoir des mixins qui sont mergés localement aux composants, et on peut avoir aussi des mixins globaux qui sont injectés dans l'instance globale du Vue, et qui influence donc toute l'application (ce dernier sont a utiliser avec précaution)

# Filters (aka vuejs pipes)

https://vuejs.org/v2/guide/filters.html

Example (self explaining):

Résultat : `NO ONE CARES, BRO !`


```html

<template>
    <p>{{ comment | reply('bro')  | shout | exclaim }}</p>
</template>

<script>
   export default {
     data() {
       return {
         comment: 'no one cares'
       }
     },
     filters: {
       shout(comment) {
         return comment.toUpperCase()
       },
       exclaim(comment) {
         return comment + '!!!'
       },
       reply(comment, name) {
         return comment + ', ' + name
       }
     }
   }
   </script>

```

Un cas d'utilisation plus réel est le formatage de date. La date sera stockée dans les données en format ISO-8601 et on voudra la présenter de manière plus 'user friendly' :

```html

```    


```html
<html lang="en-US">
<body>

	<div  id="my_app">
	    <p>
        La date formatée {{ maDate | dateFormat }}
      </p>
	</div>


   <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
   <script>
        let app = new Vue({
            el: "#my_app",
            data: {
              maDate: "2019-11-22T11:10:52+00:00"
            },
            filters: {
              dateFormat(value) {
                const date = new Date(value);
                return date.toLocaleString(['fr-CH'], {
                  dateStyle: 'full',
                 timeStyle: 'full'
                });
              }
            }
        });
   </script>
</body>
</html>
```  

A noter qu'on peut déclarer des filtres globaux qui seront disponibles pour chaques composants.

**Attention** : A priori, il serait plus efficient de se cantonner à utiliser des methods ou computer properties et de laisser un peu les filtres de côté.
