# Routing

Une application SPA, va charger une seule page HTML, puis se mettre à jour seule par la suite, via son javascript.

En VueJS, dans le mode SPA, quand un utilisateur ouvre un lien (p.ex http://host/mon_app/lien) le routage intervient au niveau du browser, dans l'application javascript : c'est le "Client Side Routing".

https://router.vuejs.org/installation.html

La mise en place du routing dans l'app se fait la plupart du temps via le "vue cli", qui met les dépendance et fait le scafolding nécessaire :
- `vue-router` est ajouté comme dépendance dans `package.json`
- `router.js` se plug dans Vue (`Vue.use`) et instantie la définition des routes : Quel composant instantier pour quelle route.
- Le point d'entrée `main.js` importe `router.js` et passe la définition dans l'instance principale et instantie le
- composant racine `App.vue` : C'est à cet endroit que les composants de routage (`router-link`, `router-view`) sont appelés :

```HTML
<div id="app">
  <div id="nav">
    <router-link to="/">Home</router-link> |
    <router-link to="/about">About</router-link>
  </div>
  <router-view/>
</div>
```

`router-link` définit les liens, et `router-view` et les placeholder ou sera affiché le composan correspondant, qui est définit dans `router.js`.

Il est a noter que le scafolding de `vue cli` place les composants des links, dans un répertoire `views`, mais qu'on peut organiser cela comme on veut : p.ex dans un répertoire `pages` ou tout mettre dans components.

`router-link` est automatiquement affecté avec les classes CSS `router-link-active` quand un lien est courramment actif on peut égalemnet lui dire de se rendre comme un `<li>` si on veut l'afficher comme une liste (see https://router.vuejs.org/api/#router-link).

## Named route

Pour éviter de mettre le chemin effectif des routes dans les composants, on peut utiliser le nom, et du coup centraliser le chemin effectif dans `router.js`. Le nom dans `router.js` devant matcher celui dans `router-link`

```js
import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import About from './views/About.vue';
import NotFound from './components/404.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    { path: '*', component: NotFound }
  ]
})
```

```html
<div id="app">
  <div id="nav">
    <router-link :to="{ name: 'home' }">Home</router-link> |
    <router-link :to="{ name: 'about' }">About</router-link>
  </div>
  <router-view/>
</div>
```

## Redirect and alias

Si on change une URL, mais qu'on ne veut pas casser le liens précédemment enregistrés par
nos visiteurs, on peut utiliser :
- Le redirect
- Le alias

See https://router.vuejs.org/guide/essentials/redirect-and-alias.html#redirect

Pour ne pas être pénaliser par les moteurs de recherche (SEO), il faut éviter d'avoir deux liens qui pointent sur la même
page : on préfèrera donc le redirect.

## History mode

Par défaut, le router vue est configuré pour que les différentes URL soient de type http://host/#/sous/lien

Le problème, c'est que ce genre d'URL sont moches, mais on peut configurer le routeur en mettant l'option `mode: 'history'`

Cela permet d'avoir des url de type http://host/sous/lien

Attention cependant, il faudra configurer le serveur de production pour que toutes les routes convergent vers index.html

https://router.vuejs.org/guide/essentials/history-mode.html

## URL dynamiques

Notre application voudra naturellement ouvrir des liens tels quel http://hosts/papers/12

On voudra évidemment instantié un composant "paper" en lui passant le paramètre 12, de façon à ce qu'il fasse une requête REST.

See
- https://router.vuejs.org/guide/essentials/dynamic-matching.html
- https://router.vuejs.org/guide/essentials/passing-props.html

```js
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { name: "home", path: '/', component: Hello }, // No props, no nothing
    { name: "hello",path: '/hello/:name', component: Hello, props: true }, // Pass route.params to props    
  ]
})
```

```html
<div id="app">
  <div id="nav">
    <router-link :to="{ name: 'home' }">Home</router-link> |
    <router-link :to="{ name: 'hello' }">Hello</router-link>
  </div>
  <router-view/>
</div>
```

```html
<template>
      <div class="user">
        <h1>{{ name }}</h1>
      </div>
</template>

<script>
    export default {
      props: ["name"]
    };
</script>
```

## Route guard hooks

https://router.vuejs.org/guide/advanced/navigation-guards.html

Il s'agit d'une série d'évènement qui sont déclenché lors de navigation. Ils peuvent se définir :
- de manière globale
- au niveau de la définition des routes
- dans les composants

A noter que les evènements 'before[...]' n'ont pas accès à `this` car à ce moment le composant n'existe pas encore.




# Global state management (vuex)

https://vuex.vuejs.org/

Si on a un ensemble de composant **éloignés logiquement les uns des autres** qui maintiennent chacun leur état (p.ex "posts", "comments", "logged in user") cela devient difficile a gérer et a propager les changements de manière intelligible au sein de l'application.

Quand utiliser vuex : see https://markus.oberlehner.net/blog/should-i-store-this-data-in-vuex/

Vuex fait office de store central d'état : il est réactif et constitue la "central source of truth". Il est basé sur la pattern établie par facebook : https://facebook.github.io/flux/docs/in-depth-overview

## Basic store without event

Un pattern plus simple serait d'utiliser notre propre store ultra simple :

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

<div  id="my_app">
    Global click count in our basic state store {{ clickCount }}

    <my_button></my_button>
    <my_button></my_button>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script>
    Vue.component("my_button", {
            data: function() {
                return {
                    clickCount: 0
                }
            },
            template: '<button v-on:click="recordClick">This button was clicked {{ clickCount }} times.</button>',
            methods: {
                recordClick: function() {
                    this.clickCount++;
                    Vue.prototype.$mystore.state.clickCount++;
                }
            }
        }
    );

    Vue.prototype.$mystore = {
        state: {
            clickCount: 0
        }
    };

    let app = new Vue({
        el: "#my_app",
        data: Vue.prototype.$mystore.state
    });
</script>
</body>
</html>
```
### Basic store with events (EventBus)

In `services/eventbus.js` :

```js

import Vue from 'vue';

class EventBusService {
    constructor() {
        this._bus = new Vue()
        this._state = Object;
    }

    get state() {
        return this._state;
    }

    $on(...args) {
        this._bus.$on(...args);
    }

    $once(...args) {
        this._bus.$once(...args);
    }

    $emit(event, ...args) {
        this._bus.$emit(event, ...args);
    }

}

const bus = new EventBusService()

export default bus;

```

Ensuite, dans les composants :

```html
<script>
  import eventbus from '@/common/eventbus.service';

  eventbus.$on( "my-event", (arg) => {
    console.log("my-event received " + arg);
  })
</script>
```

### Vuex store core concepts

Un store vuex est plus complexe et est composé :
- D'une seule instance au sein de l'application
- de `state` : qui est fait le `data` du vuex
- de `getter` : qui sont des sortes de computed properties du vuex
- de `mutations` : qui servent à modifier le state global (synchrone)  (`this.$store.commit('MUTATION_NAME')`)
- d' `actions` : permet d'encapsuler de la logique et appellent des mutations. Les actions sont asynchrone. (`this.$store.dispatch('actionName')`)

En gros :
- Le composant lit les données du store en le lisant directement ou via leur getter
- Le composant modifie les données en dispatchant des 'actions' qui elles même invoquent des mutations.

Pour voir des examples :
- `git clone https://github.com/vuejs/vuex.git && cd vuex && yarn install && yarn run dev`

Ceci est une introduction, il faudra revenir sur les cours https://www.vuemastery.com/courses/mastering-vuex a mesure que j'en aurai besoin.


A retenir :
- **Bien réfléchir à ce qu'on stock dans le store** : p.ex auth token, pref utilisateurs, erreurs de l'applications, logger
- **Privilégier des composants autonomes** : props, slots, etc.


Pour un exemple de gestion centrale d'erreurs : https://github.com/Code-Pop/real-world-vue/releases/tag/lesson15-notifications-finish
