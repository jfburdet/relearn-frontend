# Authentication

On va utiliser le JWT, tels que déjà implémentés dans le proto de backend sci2sci.

Rappel du process JWT :
- Le user envoi nom/mot de passe en faisant un POST sur /api/auth
- En cas de succès, le serveur envoi le token de validation `{ token: "slkjhldiuzlbgdhlkjhdg"}` qui sert a authentifier les requête suivantes. On stockera le token dans le stockage local du browser.
