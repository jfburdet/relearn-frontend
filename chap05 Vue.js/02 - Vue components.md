# Vue Components

## Introduction

Les composant Vue sont des éléments autonomes qui composent l'application sous forme d'une arborescence.

Ils permette de modulariser l'application et de favoriser le code-reuse.

On déclare un composant de cette façon :

```js
Vue.component('mon_bouton') , {
  /* options object pour définir le composant */
});
```

Cela **enregistre** le composant au sein de Vue avec l'id `mon_bouton`.

L'objet d'option pour définir le composant contiendra :
- Un template d'HTML: attention, il doit contenir un seul élément racine, de façon
  à ce que le composant puisse s'y attacher.
- Une **fonction** retournant l'objet de data (comme ça chaque instance du composant aura son instance autonome de data)
- Le reste des méthodes déjà vue (ainsi que d'autres): `methods`, `computed`, etc
- Un objet spécifiant les **paramètres** du composant, les **props**.

### Composant basique

```HTML
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

<div  id="my_app">
    <my_button></my_button>
    <my_button></my_button>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script>
    Vue.component("my_button", {
            data: function() {
                return {
                    clickCount: 0
                }
            },
            template: '<button v-on:click="clickCount++">This button was clicked {{ clickCount }} times.</button>'
        }
    );

    let app = new Vue({
        el: "#my_app"
    });
</script>
</body>
</html>
```

Le processus est claire :
- On définit l'app
- Elle se bind sur le div "my_app"
- La div contient des références sur des composant "my_button", qui sont instantiés et
  se comporte de manière autonomes.

### Composant avec un paramètre (props) / communication parent->enfant

Voici maintenant le même exemple, mais avec l'utilisation de "props", c.a.d. de paramètres
du composant.

Les props permettent de communiquer une information de composants **parents -> enfants**.

Evidemment le props peut être un type basique (string, entier) ou un objet complexe.

** Attention : ** Le composant enfant ne doit pas mettre à jour la props.

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

<div  id="my_app">
    <my_button title="Le premier bouton"></my_button>
    <my_button title="Le second bouton"></my_button>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script>

    Vue.component("my_button", {
            props: ['title'],
            data: function() {

                return {
                    clickCount: 0
                }
            },
            template: '<button v-on:click="clickCount++">{{ title }} was clicked {{ clickCount }} times.</button>'
        }
    );

    let app = new Vue({
        el: "#my_app"
    });
</script>
</body>
</html>
```

### Composants instantiés avec v-for

Supposons que nous voulons maintenant un nombre de composant dynamique, pex en fonction
d'une réponse a une requète REST.
Cela se réalise facilement via une boucle v-for et des v-bind (rappel: v-bind:title s'abrège :title)

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

<div id="my_app">
    <my_button
            :key="btn.id"
            :title="btn.title"
            v-for="btn in les_boutons"
    ></my_button>

</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script>

    Vue.component("my_button", {
            props: ['title'],
            data: function () {

                return {
                    clickCount: 0
                }
            },
            template: '<button v-on:click="clickCount++">{{ title }} was clicked {{ clickCount }} times.</button>'
        }
    );

    let app = new Vue({
        el: "#my_app",
        data: {
            les_boutons: [
                {id: 1, title: "Le premier bouton"},
                {id: 2, title: "Le second bouton"}
            ]
        }Voici un exemple complet, avec également une vérification de la validité des champs.
    });
</script>
</body>
</html>
```

### Child events / communication enfants -> parent

Supposons que nous voulions que nous instance vue dispose d'un compteur qui totalise
le total des clicks des enfants.

Comment cette communication enfants -> parent se fait en Vue ?

Simplement avec la notion d'events :

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

<div id="my_app">
    <my_button
            :key="btn.id"
            :title="btn.title"
            v-for="btn in les_boutons"
            @was_clicked="updateTotal"
    ></my_button>

    <p>Total de click {{ clickTotal }}</p>

</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script>

    Vue.component("my_button", {
            props: ['title'],
            data: function () {

                return {
                    clickCount: 0
                }
            },
            methods: {
                recordClick: function() {
                    this.clickCount++;
                    this.$emit('was_clicked')
                }
            },
            template: '<button v-on:click="recordClick">{{ title }} was clicked {{ clickCount }} times.</button>'
        }
    );

    let app = new Vue({
        el: "#my_app",
        data: {
            les_boutons: [
                {id: 1, title: "Le premier bouton"},
                {id: 2, title: "Le second bouton"}
            ],
            clickTotal: 0
        },
        methods: {
            updateTotal: function() {
                this.clickTotal++;
            }
        }
    });
</script>
</body>
</html>
```

## input/forms et double-way databinding (`v-model`)

Voici un exemple d'un composant qui enregistre un feedback utilisateur et qui utilise le double-way binding avec la directirve `v-model`.

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

<div id="my_app">
    <my_review_form title="Review1" @submited="addReview"></my_review_form>

    <p>Nombre de reviews enregistrées {{ reviews.length }}</p>

</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/lodash@4.13.1/lodash.min.js"></script>
<script>

    Vue.component("my_review_form", {
            props: ['title'],
            data: function () {
                return {
                    errors: [],
                    authorName: "",
                    content: ""
                }
            },
            methods: {
                onSubmit() {
                    if (_.every([this.authorName, this.content], Boolean)) {
                        this.$emit("submited", {"authorName": this.authorName, "content": this.content});
                    } else {
                        if (!this.authorName) this.errors.push("auhtor field can not be empty");
                        if (!this.content) this.errors.push("content field can not be empty");
                    }
                }
            },
            template: `
            <form @submit.prevent="onSubmit">
                <div v-if="errors.length>0" class="errors">
                    <p v-for="err in errors">{{err}}</p>
                </div>

                <p>
                  <label for="name">Name:</label>
                  <input id="name" v-model="authorName">
                </p>

                <p>
                  <label for="review">Review:</label>
                  <textarea id="review" v-model="content"></textarea>
                </p>

                 <p>
                    <input type="submit" value="Submit">
                 </p>
            </form>
            `
        }
    );

    let app = new Vue({
        el: "#my_app",
        data: {
            reviews: []
        },
        methods: {
            addReview: function (rev) {
                console.log("Review recue " + rev);
                this.reviews.push(rev);
            }
        }
    });
</script>
</body>
</html>
```

## grand-child to parent communication / global state

Si l'arborescence de composants commence à s'étoffer, on va commencer a avoir besoin de gérer des états "globaux". Un composant enfant aura du mal à faire remonter un évènement via tous ses parents sans rendre l'application spaghetti.

Il existe plusieurs moyen de réaliser ceci. Quand l'application reste petite, on peut par exemple utiliser un **eventBus** (cf ci-dessous). Pour des applications plus grandes, il faudra utiliser le gestionnaire d'état **Vuex** qui aura un chapitre dédié.

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

<div id="my_app">
    <p>Nombre de click enregistrées {{ clickCount }}</p>

    <my-button-container></my-button-container>

</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script>

    var eventBus = new Vue();

    Vue.component("my-button-container", {
        data: function () {
            return {
                buttonsNames: ["Premier", "Deuxieme", "Troisieme"]
            }
        },
         template: `
            <div>
                <p> Voici la liste de mes enfants : </p>
               <my-button
                   v-for="(btnName, btnIndex) in buttonsNames"
                   :title="btnName"
                   :key="btnIndex"
                ></my-button>
            </div>
           `
    });

    Vue.component("my-button", {
        props: ["title"],
        data: function () {
            return {
                clickCount: 0
            }
        },
        methods: {
            recordClick: function() {
                this.clickCount++;
                eventBus.$emit('a-button-was-clicked');
            }
        },
        template: '<button v-on:click="recordClick">{{ title }} was clicked {{ clickCount }} times.</button>'
    });

    let app = new Vue({
        el: "#my_app",
        data: {
            clickCount: 0
        },
        mounted() {
            eventBus.$on('a-button-was-clicked', () => {
                console.log("eventBus handler called.")
                this.clickCount++;
            })
        }
    });
</script>
</body>
</html>
```

## Vue Slots

Les slots sont une façon alternative de composer des composants. Sans la notion de slots, un composant est responsable à 100% de son contenu. Avec les slots, le composant peut dire qu'une certaine de ses parties sont générées par le composant **parent** qui l'instantie.

See https://flaviocopes.com/vue-slots/

Les slots peuvent être un outils très avancé de composition, à creuser par la suite : https://vuejs.org/v2/guide/components-slots.html#Other-Examples

Consulter encore la doc pour découvir :
- Les slots multiples et nommés (https://vuejs.org/v2/guide/components-slots.html#Named-Slots)
- Les scoped slots : un attribut de l'enfant est exposé au parent https://vuejs.org/v2/guide/components-slots.html#Scoped-Slots


Voici un exemple simple.

```HTML
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .framed {
            border:1px solid black;
        }
    </style>
</head>
<body>

<div id="my_app">

    <mon-composant title="première instance">
        <p>
            Toute cette partie sera passée au composant enfant et ira
            dans son slot.
        </p>
    </mon-composant>

    <mon-composant title="Deuxième (no slot param passed)">
    </mon-composant>

</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script>

    Vue.component("mon-composant", {
        data: function () {
            return {
                clickCount: 0
            }
        },
        props: ["title"],
         template: `
            <div class="framed">
                <p> {{ title }} : Voici la partie que je gère  </p>
                <button v-on:click="clickCount++">I was clicked {{ clickCount }} times.</button>

                <p> Et ci-dessous la partie passée par mon parent</p>
                <slot>Ceci est affiché si aucun paramètre 'slot' n'est passé (default fallback)</slot>
            </div>
           `
    });

    let app = new Vue({
        el: "#my_app",
    });
</script>
</body>
</html>
```

## Single file components

Jusqu'à présent on a instantié directement nos composants. Via le `vue cli` qui configure
un environement webpack, on pourra déclarer un composant au sein d'un fichier `.vue`

La structure de base, se compose en trois sections :

```html
<template>
    <div></div>
</template>

<script>
  export default { }
</script>

<style scoped>
</style>
```

Chaque section est chargée par un `vue-loader` (see https://vue-loader.vuejs.org/) et par défaut, les languages utilisés sont :
- html
- javascript
- css

Mais on pourrait très bien configurer d'autres loader pour avoir p.ex le triplet pug, typescrit, scss

**Attention** : La section `template` ne doit contenir qu'un seul élément racine.

L'utilisation de single file components est la façon "standard" de composer une grosse application.

## Scoped styles / global style

Quand on utilise l'attribut `scoped` dans la section `style`, le css définit à l'intérieur ne s'applique **QUE** au template du composant (via une ré-écriture des style utilisant des id uniques : à tester avec les devtools)

Si on désire avoir des style globaux à l'application, une façon de faire et de les mettre dans la section `style` du composant racine `App.vue`.

D'autre façon de faire sont :
- Dans `App.vue`, faire un import :
```
 <style>
  @import './assets/styles/global.css';
</style>
```
- dans le fichier `main.js` faire ``

## Nesting components

Notre application sera une arborescence de composants, dont la racine sera `App.vue`. Comment instantier des composants enfants ?

- Dans la section javascript, il faut importer sa définition, puis exporter pour le rendre disponible dans `template`
- Dans template, on peut l'utiliser

```html
<template>
  <div>
    <SousComposant title="A nice title"/>
  </div>
</template>

<script>
// @ is an alias to /src
import SousComposant from '@/components/SousComposant.vue'

export default {
  name: 'home',
  components: {
    SousComposant
  }
}
</script>
```

## Composants globaux

Si un composant est utilisé souvent dans l'application, il peut devenir laborieux de l'importer à chaque fois: pour éviter ceci, on peut l'enregistrer comme composant global.

On peut les enregister a la main dans `main.js` ou utiliser un système plus automatisé.

Une fois enregistrés, on peut les utiliser dans n'importe quel autre composant, sans avoir à réaliser l'étape d'importation.

https://vuejs.org/v2/guide/components-registration.html
