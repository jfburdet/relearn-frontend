# Les autres concepts de base CSS

## Les pseudo-classes

Permettent de sélectionner des éléments selon leur **état** (p.ex un checkbox checked ou pas) ou pour sélectionner un enfant **spécifique** dans le dom.

p.ex
- **:active** : élément activé (p.ex cliqué)
- **:empty** : élément sans enfant
- **:link** : un link qu'on a pas encore cliqué

See [https://flaviocopes.com/css-pseudo-classes/](https://flaviocopes.com/css-pseudo-classes/)

## Les pseudo éléments

See [https://flaviocopes.com/css-pseudo-elements/](https://flaviocopes.com/css-pseudo-elements/)

Permet de cible une partie spécifique d'un élément.
P.ex

```css

// Formate la première ligne du paragraphe avec une fonte de 2 fois celui
// assigné au root element
p::first-line {
  font-size: 2rem;
}
```

Cf également [cet example](examples/before-selector.html) en utilisant les outils développeur.
