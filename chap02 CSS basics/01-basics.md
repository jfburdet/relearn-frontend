# CSS basics

## Reférences

- [The CSS Handbook](The_CSS_Handbook_Flavio_Copes.pdf)
- https://developer.mozilla.org/en-US/docs/Learn/CSS/First_steps/How_CSS_is_structured

## Introduction

CSS est un langage qui permet de définir et de combiner des styles qui vont s'appliquer aux éléments du document HTML.

Un fichier CSS contient une énumération de **règles CSS**, chaque règle étant composée :
- d'un **sélecteur**
- d'un **block de déclarations**, qui contient une ou plusieurs **rules**.

Une **rule** est composée d'une **property** et d'une **valeure** et se termine par un **point-virgule**.

Exemple :

```CSS
p {
font-size: 20px;
}
```

La règle qui fixe l'attribut "font-size" à la valeur 20px va s'appliquer à tous les éléments HTML du document qui sont de type "p".

L'empilement de toutes les règles (les **cascading**) finira par document le style des tous les éléments du documents.

## Adding CSS to html

### Using link in header

```html
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" type="text/css" href="myfile.css">
  </head>
</html>
```

### Using style tag in header
```html
<html>
  <head>
    <style>
      h1 {color:red;}
      p {color:blue;}
    </style>
  </head>
</html>
```

### Using inline styles

On affecte la propriété 'style' dans la déclaration HTML de notre élément :
```html
  <div style="background-color: yellow">...</div>
```
