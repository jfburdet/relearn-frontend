# Sélecteurs

Nous allons voir les divers sélecteurs qui permettent de déterminer quel noeud du DOM est concerné par la règle.

Cette section n'est pas exhaustive Ne pas hésiter à googler ou a utiliser des live tutorial pour s'entrainer :
- [Google 'css selectors cheat shet'](https://www.google.com/search?&q=css+selectors+cheat+sheet)
- [CSS Diner](https://flukeout.github.io)

## Select d'élément'

Pour chaque type d'élément html (div, p, span, img, etc) il y a un sélecteur qui correspond, p.ex

```CSS
div {
  color: yellow;
}
```

Résultat : Tous les div du document auront l'attribut color mis à yellow.

## Sélecteur de classe :

Match l'attribut "class" des éléments du DOM.


```CSS
.dog-name {
  color: yellow;
}
```

```html
<p class="dog-name">
  Roger
</p>
```

Cette règle va affecter la couleur jaune à tous les éléments qui ont la classe "dog-name".

## Sélecteur d'id

L'attribut "id" des éléments HTML est particulier, dans le sens où il est unique.

Le sélecteur correspondant est :

```CSS
#paraph1234 {
  color: yellow;
}
```

```html
<p id="paraph1234">
  Roger
</p>
```

## Combiner les Sélecteurs

```CSS
/* match toutes les divs qui ont la classe 'dog-name' */
div.dog-name {
  color: yellow;
}
```
```CSS
/* match toutes la div qui a l'id 'dog-name' */
div#dog-name {
  color: yellow;
}
```

On poussant un peu, la règle suivante ("donne moi les div qui ont la classe dog-name et la classe toto et qui ont l'id special1234") :

```CSS
div.dog-name.toto#special1234 {
  color: yellow;
}
```

matchera le html suivant :

```html
<div class="dog-name toto" id="paraph1234">
  Roger
</div>

<!-- Ou celui-ci aussi ! -->
<div class="toto dog-name" id="paraph1234">
  Roger
</div>
```

## Interlude : Comment l'exemple précédent dans le navigateur.

Ouvrir le fichier [ordre-multi-class.html](examples/ordre-multi-class.html) et utiliser les outils Web Developer (F12) pour explorer le DOM : On voit que les deux 'p' sont en jaune. Ils ont été sélectionné quelquue soit l'ordre des classes.

### Sélecteur de hiérarchie

```CSS
/* Sélectionne n'importe quel span se trouvant parmis les enfants (quelque soit le niveau de profondeur) d'un p */
p span {
  color: yellow;
}
```

```CSS
/* Sélection les span qui sont directement enfant d'un p */
p > span {
  color: yellow;
}
```

## Sélecteur 'sibling'

```CSS
p + span {
  color: yellow;
}
```

Sélectionne les span qui sont précédé au même niveau dans le DOM, d'un p.

P.ex
```html
<p>This is a paragraph</p>
<span>This is a yellow span</span>
```

## Sélecteur d'attribut

| Sélecteur        | Résultat           |
| ------------- |-------------|
| p[toto]    | Tous les p contenant attribut toto |
| p[toto="val"]     | Tous les p contenant attribut toto valant 'val' |
| p[toto*="val"]     | Tous les p contenant attribut toto contenant 'val' |
| p[toto^="val"]     | Tous les p contenant attribut toto commençant par 'val' |
| p[toto$="val"]     | Tous les p contenant attribut toto terminant par 'val' |


## Groupage des Sélecteurs

Si on veut écrire plusieurs sélecteurs qui utilisent le même bloc, pas besoin de se répéter !

Ceci :
```css
p, .dog-name {
  color: yellow;
}

/* ou avec un retour de ligne pour plus de clarté */
p,
.dog-name {
  color: yellow;
}
```

Est équivalent à :
```css
.dog-name {
  color: yellow;
}

p {
  color: yellow;
}
```
