# Règles de cascade

Un fichier CSS contient plusieurs sélecteurs dont les règles peuvent s'appliquer à plusieurs éléments.

Les règles de cascade vont permettre de déterminer quel règle de style est effectivement attribuée à chaque noeud lorsque plusieurs sélecteur conflictuels s'appliquent.

Il faut savoir qu'en plus du fichier CSS que la page web référence, le browser à son propre jeu de règle de base et les plugins peuvent en appliquer en plus.

Les règles de cascade tiennent compte :
- de la spécificité
- du tag !important
- de l'héritage

See :
- https://flaviocopes.com/css-specificity/
- https://specificity.keegan.st/
- https://www.sitepoint.com/css-inheritance-introduction/

## Spécificité

Si plusieurs règles s'appliquent, chacune se voit attribuer un poids qui est d'autant plus grand quel cible précisément/explicitement un noeud.

**Recommandations** : Le monde CSS s'accorde à dire qu'il faut privilégier les sélecteur de classes en lieu et place des sélecteur d'id ou (pire) du tag 'importance', ce dernier étant à proscrire.

**element  < class/attribute < id < inline style < importance**

|Sélecteur   |Score |
|---|---|
|p {} 					    | 0 0 0 1 |
|span {} 				  | 0 0 0 1 |
|p span {} 			  | 0 0 0 2 |
|p > span {} 			| 0 0 0 2 |
|div p > span {} 	| 0 0 0 3 |
|||
|.name {}				  | 0 0 1 0 |
|.users .name {}		| 0 0 2 0 |
|[href$='.pdf'] {}	| 0 0 1 0 |
|:hover {}				  | 0 0 1 0 |
|||
|.name {}				    | 0 0 1 0 |
|.name.name {}		    | 0 0 2 0 |
|.name.name.name {}	| 0 0 3 0 |
|||
|div .name {}			  	  | 0 0 1 1 |
|a[href$='.pdf'] {}		  | 0 0 1 1 |
|.pictures img:hover {}  | 0 0 2 1 |
|||
|#name {}					| 0 1 0 0 |
|.user #name {}		| 0 1 1 0 |
|#name span {}			| 0 1 0 1 |
|||
|&lt;p style="color: red"&gt;Test&lt;/p&gt; | 1 0 0 0 |

On peut également spécifier le tag "importance", qui rend la règle prioritaire (ensuite les règles de spécificités s'appliquent pour départager plusieurs règles prioritaires)

```CSS
p {
  font-size: 20px!important;
}
```

## Héritage

Quand cela est cohérent avec leur usage, certaines propriété s'hérite de la hiérarchie. P.ex si on assign 'font-family' à la racine html (au body), c'est logique que la fonte soit héritée à tous les éléments, comme ça ca nous évite de devoir la respécifier.

Pour d'autre, cela ferait moins sens (p.ex background-color)

Voici les proriété les plus courantes qui héritent :

||||
|---|---|---|
|	border-collapse	|	font-size-adjust	|	tab-size	|
|	border-spacing	|	font-stretch	|	text-align	|
|	caption-side	|	font	|	text-align-last	|
|	color	|	letter-spacing	|	text-decoration-color	|
|	cursor	|	line-height	|	text-indent	|
|	direction	|	list-style-image	|	text-justify	|
|	empty-cells	|	list-style-position	|	text-shadow	|
|	font-family	|	list-style-type	|	text-transform	|
|	font-size	|	list-style	|	visibility	|
|	font-style	|	orphans	|	white-space	|
|	font-variant	|	quotes	|	widows	|
|	font-weight	|		|	word-break	|
|		|	|	word-spacing	|

On peut **forcer** une propriété non-héritable à être héritée de la hiérarchie:
```CSS
body {
  background-color: yellow;
}
p {
  background-color: inherit;
}
```
